package controller.serialization.json;

import com.google.gson.reflect.TypeToken;
import model.Crime;
import model.people.PersonBuilder;
import model.people.Prisoner;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

/** Test serialization of prisoners. */
public class PrisonerSerializerTest extends GsonSerializerTest<Prisoner> {

    private static final Type TYPE = new TypeToken<Prisoner>() {}.getType();
    private static final Type SET_TYPE = new TypeToken<Set<Prisoner>>() {}.getType();
    private static final Type LIST_TYPE = new TypeToken<List<Prisoner>>() {}.getType();

    @Override
    protected Prisoner buildT() {
        return new PersonBuilder("Pippo", "Pippo", LocalDate.now())
                .buildPrisoner(0, 0, LocalDate.now(), LocalDate.MAX)
                .addCrimes(EnumSet.allOf(Crime.class))
                .build();
    }

    @Override
    protected Type getType() {
        return TYPE;
    }

    @Override
    protected Type getSetType() {
        return SET_TYPE;
    }

    @Override
    protected Collection<Prisoner> buildCollectionT() {
        final Prisoner p1 = buildT();
        final Prisoner p2 = new PersonBuilder("Donald", "Duck", LocalDate.now())
                .buildPrisoner(1, 1, LocalDate.now(), LocalDate.now().plusDays(100))
                .addCrime(Crime.ANIMALS)
                .build();
        final Prisoner p3 = new PersonBuilder("Mickey", "Mouse", LocalDate.now())
                .buildPrisoner(2, 2, LocalDate.now(), LocalDate.MAX)
                .addCrimes(EnumSet.noneOf(Crime.class))
                .build();

        final List<Prisoner> list = new ArrayList<>();
        list.add(p1);
        list.add(p2);
        list.add(p3);
        return list;
    }

    @Override
    protected Type getListType() {
        return LIST_TYPE;
    }
}
