package controller.serialization.json;

import com.google.gson.reflect.TypeToken;
import model.people.Guard;
import model.people.PersonBuilder;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/** Test serialization of guards. */
public class GuardSerializerTest extends GsonSerializerTest<Guard> {

    @Override
    protected Guard buildT() {
        return new PersonBuilder("Pippo", "Pippo", LocalDate.now())
                .buildGuard(0, "0123456789", 0, "password")
                .build();
    }

    @Override
    protected Type getType() {
        return new TypeToken<Guard>() {}.getType();
    }

    @Override
    protected Collection<Guard> buildCollectionT() {
        final Guard p1 = buildT();
        final Guard p2 = new PersonBuilder("Donald", "Duck", LocalDate.now())
                .buildGuard(1, "9876543210", 1, "qwertyqwerty")
                .build();
        final Guard p3 = new PersonBuilder("Mickey", "Mouse", LocalDate.now())
                .buildGuard(2, "9876543210", 2, "asdasd")
                .build();

        final List<Guard> list = new ArrayList<>();
        list.add(p1);
        list.add(p2);
        list.add(p3);
        return list;
    }

    @Override
    protected Type getSetType() {
        return new TypeToken<Set<Guard>>() {}.getType();
    }

    @Override
    protected Type getListType() {
        return new TypeToken<List<Guard>>() {}.getType();
    }
}
