package controller.serialization.json;

import com.google.gson.reflect.TypeToken;
import model.Cell;
import model.CellImpl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/** Test serialization of cells. */
public class CellSerializerTest extends GsonSerializerTest<Cell> {

    @Override
    protected Cell buildT() {
        return new CellImpl(1, "A", 1);
    }

    @Override
    protected Type getType() {
        return new TypeToken<Cell>() {}.getType();
    }

    @Override
    protected Collection<Cell> buildCollectionT() {
        final Cell c1 = buildT();
        final Cell c2 = new CellImpl(2, "B", 2);
        final Cell c3 = new CellImpl(3, "C", 3);
        final List<Cell> list = new ArrayList<>();
        list.add(c1);
        list.add(c2);
        list.add(c3);
        return list;
    }

    @Override
    protected Type getSetType() {
        return new TypeToken<Set<Cell>>() {}.getType();
    }

    @Override
    protected Type getListType() {
        return new TypeToken<List<Cell>>() {}.getType();
    }
}
