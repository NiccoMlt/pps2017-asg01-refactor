package controller.serialization.json;

import com.google.gson.reflect.TypeToken;
import model.Movement;
import model.MovementImpl;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/** Test serialization of movements. */
public class MovementSerializerTest extends GsonSerializerTest<Movement> {
    @Override
    protected Movement buildT() {
        return new MovementImpl("Pippo", new BigDecimal(1));
    }

    @Override
    protected Type getType() {
        return new TypeToken<Movement>() {}.getType();
    }

    @Override
    protected Collection<Movement> buildCollectionT() {
        final Movement m1 = buildT();
        final Movement m2 = new MovementImpl("Pluto", new BigDecimal(0), LocalDateTime.MIN);
        final Movement m3 = new MovementImpl("Paperino", new BigDecimal(-1));
        final Movement m4 = new MovementImpl("Topolino", new BigDecimal("-4.5"), LocalDateTime.MAX);

        final List<Movement> list = new ArrayList<>();
        list.add(m1);
        list.add(m2);
        list.add(m3);
        list.add(m4);
        return list;
    }

    @Override
    protected Type getSetType() {
        return new TypeToken<Set<Movement>>() {}.getType();
    }

    @Override
    protected Type getListType() {
        return new TypeToken<List<Movement>>() {}.getType();
    }
}
