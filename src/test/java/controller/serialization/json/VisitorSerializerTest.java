package controller.serialization.json;

import com.google.gson.reflect.TypeToken;
import model.people.PersonBuilder;
import model.people.Visitor;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/** Test serialization of visitors. */
public class VisitorSerializerTest extends GsonSerializerTest<Visitor> {

    @Override
    protected Visitor buildT() {
        return new PersonBuilder("Pippo", "Pippo", LocalDate.now())
                .buildVisitor(0)
                .build();
    }

    @Override
    protected Type getType() {
        return new TypeToken<Visitor>() {}.getType();
    }

    @Override
    protected Type getSetType() {
        return new TypeToken<Set<Visitor>>() {}.getType();
    }

    @Override
    protected Collection<Visitor> buildCollectionT() {
        final Visitor v1 = buildT();
        final Visitor v2 = new PersonBuilder("Donald", "Duck", LocalDate.now())
                .buildVisitor(1)
                .build();
        final Visitor v3 = new PersonBuilder("Mickey", "Mouse", LocalDate.now())
                .buildVisitor(2)
                .build();

        final List<Visitor> list = new ArrayList<>();
        list.add(v1);
        list.add(v2);
        list.add(v3);
        return list;
    }

    @Override
    protected Type getListType() {
        return new TypeToken<List<Visitor>>() {}.getType();
    }
}
