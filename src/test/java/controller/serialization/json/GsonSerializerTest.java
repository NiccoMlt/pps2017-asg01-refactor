package controller.serialization.json;

import controller.serialization.Serializer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class GsonSerializerTest<T> {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();
    private Serializer serializer;

    @Before
    public void buildGsonSerializer() {
        serializer = new GsonSerializer();
    }

    @Test
    public void jsonTest() throws IOException {
        final File file = folder.newFile();
        final T toSerialize = buildT();

        serializer.serializeJSON(toSerialize, getType(), file.getPath());
        final T deserialize = serializer.deserializeJSON(getType(), file.getPath());

        Assert.assertEquals(toSerialize, deserialize);
    }

    @Test
    public void collectionsTest() throws IOException {
        final File file = folder.newFile();

        final Set<T> set = buildSetT();
        serializer.serializeJSON(set, getSetType(), file.getPath());
        Assert.assertEquals(set, serializer.deserializeJSON(getSetType(), file.getPath()));

        final List<T> list = buildListT();
        serializer.serializeJSON(list, getListType(), file.getPath());
        Assert.assertEquals(list, serializer.deserializeJSON(getListType(), file.getPath()));
    }

    protected  abstract T buildT();

    protected abstract Type getType();

    private Set<T> buildSetT() {
        return new HashSet<>(buildCollectionT());
    }

    protected abstract Collection<T> buildCollectionT();

    protected abstract Type getSetType();

    private List<T> buildListT() {
        return new ArrayList<>(buildCollectionT());
    }

    protected abstract Type getListType();
}