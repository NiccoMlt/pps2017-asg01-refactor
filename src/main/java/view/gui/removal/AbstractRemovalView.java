package view.gui.removal;

import controller.view.RemovalController;
import view.ObjectRemovalView;
import view.PrisonView;
import view.gui.AbstractPrisonView;

import javax.annotation.Nullable;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * The class models an abstract implementation of a view in Swing that removes a generic object.
 *
 * @param <T> {@inheritDoc}
 */
public abstract class AbstractRemovalView<T> extends AbstractPrisonView implements ObjectRemovalView<T> {

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param title      the title of the view
     * @param controller the controller of the view
     */
    public AbstractRemovalView(final int rank, final @Nullable PrisonView parent, final String title,
                               final RemovalController<T> controller) {
        super(rank, parent);

        final JLabel titleLabel = new JLabel(title);
        addToNorth(titleLabel);

        final JButton remove = new JButton("Rimuovi");
        remove.addActionListener(e -> {
            if (controller.remove(getID())) {
                displayInfoMessage(title, "Rimozione completata");
            } else {
                displayErrorMessage(title, "Impossibile rimuovere");
            }
        });
        addButtonsToSouth(remove);
    }

    @Override
    public abstract int getID();
}
