package view.gui.removal;

import controller.view.RemovalController;
import model.people.Guard;
import view.ObjectRemovalView;
import view.PrisonView;

import javax.swing.JLabel;
import javax.swing.JTextField;

/** View that removes a guard. */
public class RemoveGuardView extends AbstractRemovalView<Guard> implements ObjectRemovalView<Guard> {

    private final JTextField id;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public RemoveGuardView(final int rank, final PrisonView parent, final RemovalController<Guard> controller) {
        super(rank, parent, "Rimozione guardia", controller);

        final JLabel idLabel = new JLabel("ID Guardia");
        id = new JTextField(2);
        idLabel.setLabelFor(id);
        addToCenter(idLabel, id);

        visible();
    }

    @Override
    public int getID() {
        final String id = this.id.getText().trim();
        return id.isEmpty() ? -1 : Integer.valueOf(id);
    }
}
