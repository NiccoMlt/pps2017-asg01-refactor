package view.gui.removal;

import controller.view.RemovalController;
import model.people.Prisoner;
import view.ObjectRemovalView;
import view.PrisonView;

import javax.swing.JLabel;
import javax.swing.JTextField;

/** View that removes a prisoner. */
public class RemovePrisonerView extends AbstractRemovalView<Prisoner> implements ObjectRemovalView<Prisoner> {

    private final JTextField id;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public RemovePrisonerView(final int rank, final PrisonView parent, final RemovalController<Prisoner> controller) {
        super(rank, parent, "Rimozione prigioniero", controller);

        final JLabel idLabel = new JLabel("ID Prigioniero");
        id = new JTextField(2);
        idLabel.setLabelFor(id);
        addToCenter(idLabel, id);

        visible();
    }

    @Override
    public int getID() {
        final String id = this.id.getText().trim();
        return id.isEmpty() ? -1 : Integer.valueOf(id);
    }
}
