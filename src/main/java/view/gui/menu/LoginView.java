package view.gui.menu;

import controller.view.LoginController;
import view.Login;
import view.gui.AbstractPrisonView;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/** This class implements a View to log a user in this application. */
public class LoginView extends AbstractPrisonView implements Login {

    private final JTextField username;
    private final JPasswordField password;
    private final LoginController controller;

    /**
     * Constructor.
     *
     * @param controller the controller
     */
    public LoginView(final LoginController controller) {
        super(0, null);
        this.controller = controller;

        final JLabel title = new JLabel("Prison Manager");
        addToNorth(title);

        final JLabel usernameLabel = new JLabel("Username");
        this.username = new JTextField(8);
        username.addActionListener(e -> username.transferFocus());
        final JLabel passwordLabel = new JLabel("Password");
        this.password = new JPasswordField(8);
        addToCenter(usernameLabel, this.username, passwordLabel, this.password);

        final JButton login = new JButton("Login");
        login.addActionListener(e -> controller.logUserIn(getUsername(), getPassword()));
        addButtonsToSouth(login);
        getFrame().getRootPane().setDefaultButton(login);

        visible();
    }

    @Override
    public String getUsername() {
        return username.getText().trim();
    }

    @Override
    public String getPassword() {
        return new String(password.getPassword());
    }

    @Override
    public int getRank() {
        return this.controller.testCredentialsFor(getUsername(), getPassword());
    }
}
