package view.gui.menu;

import controller.view.MenuController;
import view.gui.insertion.AddPrisonerView;
import view.gui.removal.RemovePrisonerView;
import view.gui.view.single.PrisonerView;

import javax.swing.JButton;

/** The main menu of the application. */
public class MainView extends AbstractMenuView {

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param controller the controller of the view
     */
    public MainView(final int rank, final MenuController controller) {
        super(rank, null, "Prison Manager");

        final JButton addPrisoner = new JButton("Aggiungi prigioniero");
        addPrisoner.addActionListener(e -> {
            close();
            new AddPrisonerView(getRank(), this, controller.addPrisoner());
        });
        final JButton removePrisoner = new JButton("Rimuovi prigioniero");
        removePrisoner.addActionListener(e -> {
            close();
            new RemovePrisonerView(getRank(), this, controller.removePrisoner());
        });
        final JButton viewPrisoner = new JButton("Vedi profilo prigioniero");
        viewPrisoner.addActionListener(e -> {
            close();
            new PrisonerView(getRank(), this, controller.viewPrisoner());
        });
        addToCenter(addPrisoner, removePrisoner, viewPrisoner);

        final JButton moreFunctions = new JButton("Altre funzioni (grado 2)");
        moreFunctions.addActionListener(e -> {
            close();
            new MoreFunctionsView(getRank(), this, controller);
        });
        final JButton highRankOnly = new JButton("Funzioni riservate (Grado 3)");
        highRankOnly.addActionListener(e -> {
            close();
            new SupervisorMenuView(getRank(), this, controller);
        });
        moreFunctions.setEnabled(rank >= MoreFunctionsView.MIN_RANK);
        highRankOnly.setEnabled(rank >= SupervisorMenuView.MIN_RANK);
        addButtonsToSouth(moreFunctions, highRankOnly);

        visible();
    }
}
