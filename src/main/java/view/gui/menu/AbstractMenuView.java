package view.gui.menu;

import controller.view.gui.LoginControllerImpl;
import view.PrisonView;
import view.gui.AbstractPrisonView;

import javax.annotation.Nullable;
import javax.swing.JButton;
import javax.swing.JLabel;

/** The class models an abstract implementation of a view in Swing that shows a menu to navigate the application. */
public abstract class AbstractMenuView extends AbstractPrisonView {

    /**
     * Constructor.
     *
     * @param rank   the rank of the user
     * @param parent the parent view
     * @param title  the title of the view
     */
    public AbstractMenuView(final int rank, final @Nullable PrisonView parent, final String title) {
        super(rank, parent);

        final JLabel titleLabel = new JLabel(title);
        addToNorth(titleLabel);

        if (!getParent().isPresent()) {
            final JButton back = new JButton();
            back.setText("Esci");
            back.addActionListener(ev -> {
                this.close();
                new LoginView(new LoginControllerImpl());
            });
            addButtonsToSouth(back);
        }
    }
}
