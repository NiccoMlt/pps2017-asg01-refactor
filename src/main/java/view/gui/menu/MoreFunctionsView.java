package view.gui.menu;

import controller.view.MenuController;
import view.gui.insertion.AddMovementView;
import view.gui.insertion.AddVisitorView;
import view.gui.view.table.BalanceView;
import view.gui.view.table.CellsView;
import view.gui.view.table.VisitorsView;

import javax.swing.JButton;

/** A secondary menu accessible only by rank {@value #MIN_RANK} user or superior. */
public class MoreFunctionsView extends AbstractMenuView {
    public static final int MIN_RANK = 2;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public MoreFunctionsView(final int rank, final MainView parent, MenuController controller) {
        super(rank, parent, "Altre funzioni");

        final JButton addMovement = new JButton("Aggiungi un movimento");
        addMovement.addActionListener(ev -> {
            close();
            new AddMovementView(getRank(), this, controller.addMovement());
        });
        final JButton viewBalance = new JButton("Guarda bilancio");
        viewBalance.addActionListener(ev -> {
            close();
            new BalanceView(getRank(), this, controller.viewBalance());
        });
        final JButton viewFirstChart = new JButton("Grafico prigionieri per anno");
        viewFirstChart.addActionListener(ev -> controller.buildBarChart());
        final JButton viewSecondChart = new JButton("Grafico percentuale crimini");
        viewSecondChart.addActionListener(ev -> controller.buildPieChart());
        final JButton addVisitors = new JButton("Aggiungi un visitatore");
        addVisitors.addActionListener(ev -> {
            close();
            new AddVisitorView(getRank(), this, controller.addVisitor());
        });
        final JButton viewVisitors = new JButton("Controlla visitatori");
        viewVisitors.addActionListener(ev -> {
            close();
            new VisitorsView(getRank(), this, controller.viewVisitors());
        });
        final JButton viewCells = new JButton("Guarda celle");
        viewCells.addActionListener(ev -> {
            close();
            new CellsView(getRank(), this, controller.viewCells());
        });

        addToCenter(addMovement, viewBalance, viewFirstChart, viewSecondChart,
                addVisitors, viewVisitors, viewCells);

        visible();
    }
}
