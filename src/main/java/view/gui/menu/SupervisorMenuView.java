package view.gui.menu;

import controller.view.MenuController;
import view.gui.insertion.AddGuardView;
import view.gui.removal.RemoveGuardView;
import view.gui.view.single.GuardView;
import view.gui.view.table.PrisonersView;

import javax.swing.JButton;

/** A secondary menu accessible only by rank {@value #MIN_RANK} user or superior. */
public class SupervisorMenuView extends AbstractMenuView {
    public static final int MIN_RANK = 3;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public SupervisorMenuView(final int rank, final MainView parent, final MenuController controller) {
        super(rank, parent, "Funzioni riservate");

        final JButton addGuard = new JButton("Aggiungi guardia");
        addGuard.addActionListener(e -> {
            close();
            new AddGuardView(getRank(), this, controller.addGuard());
        });
        JButton removeGuard = new JButton("Rimuovi guardia");
        removeGuard.addActionListener(e -> {
            close();
            new RemoveGuardView(getRank(), this, controller.removeGuard());
        });
        JButton viewGuard = new JButton("Vedi guardia");
        viewGuard.addActionListener(e -> {
            close();
            new GuardView(getRank(), this, controller.viewGuard());
        });
        JButton viewPrisoners = new JButton("Vedi prigionieri in una frazione di tempo");
        viewPrisoners.addActionListener(e -> {
            close();
            new PrisonersView(getRank(), this, controller.viewPrisoners());
        });
        addToCenter(addGuard, removeGuard, viewGuard, viewPrisoners);

        visible();
    }
}
