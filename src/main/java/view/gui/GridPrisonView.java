package view.gui;

import com.google.common.base.Strings;
import view.PrisonView;
import view.gui.components.PrisonPanel;

import javax.annotation.Nullable;
import javax.swing.DefaultListModel;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

import static java.time.Month.JANUARY;

/** Simple implementation of {@link AbstractPrisonView} with a more customizable layout. */
public class GridPrisonView extends AbstractPrisonView implements PrisonView {
    protected static final LocalDate DEFAULT_BIRTH_DATE = LocalDate.of(1980, JANUARY, 1);
    protected static final LocalDate DEFAULT_START_DATE = LocalDate.of(2017, JANUARY, 1);
    protected static final LocalDate DEFAULT_END_DATE = LocalDate.of(2018, JANUARY, 1);

    private final PrisonPanel center;
    private final GridLayout centerLayout;
    private final PrisonPanel east;
    private final GridLayout eastLayout;

    /**
     * Constructor.
     *
     * @param rank   the rank of the user
     * @param parent the parent view
     */
    public GridPrisonView(int rank, @Nullable PrisonView parent) {
        super(rank, parent);

        this.centerLayout = new GridLayout(0, 2);
        this.center = new PrisonPanel(this.centerLayout);
        addToCenter(this.center);

        this.eastLayout = new GridLayout(0, 1);
        this.east = new PrisonPanel(this.eastLayout);
        getFrame().getContentPane().add(BorderLayout.EAST, east);
    }

    /**
     * Add a {@code List} to the east panel that contains some elements of an enum.
     *
     * @param name the optional name to give to the list, that will appear over
     * @param <E>  the enum type
     * @return the list component
     */
    protected final <E extends Enum<E>> JList<E> addList(final @Nullable String name) {
        final Optional<JLabel> nameLabel = Optional.ofNullable(
                Strings.nullToEmpty(name).trim().equals("")
                        ? new JLabel(name)
                        : null);
        final DefaultListModel<E> model = new DefaultListModel<>();
        final JList<E> list = new JList<>(model);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setLayoutOrientation(JList.VERTICAL);
        list.setVisibleRowCount(-1);

        nameLabel.ifPresent(this::addToEast);
        addToEast(new JScrollPane(list));

        return list;
    }

    /**
     * Add a component to the east panel.
     *
     * @param component the component to add
     */
    protected void addToEast(final JComponent component) {
        east.add(component);
        eastLayout.setRows(eastLayout.getRows() + 1);
    }

    /**
     * Add a {@code Label} to the center panel that contains some string (default "").
     *
     * @param name the name to give to the field, that will appear aside
     * @return the field component
     */
    protected final JLabel addTextView(final String name) {
        final JLabel label = new JLabel(name);
        final JLabel textField = new JLabel();
        label.setLabelFor(textField);
        addRowToCenter(label, textField);

        return textField;
    }

    /**
     * Add a {@code Label} to the center panel that contains some string representing a date (default "").
     *
     * @param name   the name to give to the field, that will appear aside
     * @param format the format of the date
     * @return the field component
     */
    protected final JLabel addDateView(final String name, final SimpleDateFormat format) {
        return addTextView(name + " (" + format.toLocalizedPattern() + ")");
    }

    /**
     * Add a {@code TextField} to the center panel.
     *
     * @param name the name to give to the field, that will appear aside
     * @param size the size of the field
     * @return the field
     */
    protected final JTextField addTextField(final String name, final int size) {
        final JLabel label = new JLabel(name);
        final JTextField textField = new JTextField(size);
        textField.setToolTipText(name);
        label.setLabelFor(textField);
        addRowToCenter(label, textField);

        return textField;
    }

    /**
     * Add a date-formatted {@code TextField} to the center panel.
     *
     * @param name   the name to give to the field, that will appear aside
     * @param format the date formatter
     * @return the field
     */
    protected final JTextField addDateField(final String name, final SimpleDateFormat format) {
        final JLabel label = new JLabel(name + " (" + format.toLocalizedPattern() + ")");
        final JTextField textField = new JFormattedTextField(format);
        textField.setColumns(format.toLocalizedPattern().length());
        textField.setToolTipText(name);
        label.setLabelFor(textField);
        addRowToCenter(label, textField);

        return textField;
    }

    /**
     * Add some components as a row in the center panel.
     *
     * @param components the components to add
     */
    protected final void addRowToCenter(final JComponent... components) {
        Arrays.stream(components).forEach(this.center::add);
        centerLayout.setRows(centerLayout.getRows() + 1);
    }
}
