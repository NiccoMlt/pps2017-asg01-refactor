package view.gui.view.single;

import controller.view.ObjectViewController;
import model.people.Guard;
import model.people.Person;
import view.ObjectView;
import view.PrisonView;

import javax.swing.JLabel;

/** View that represents a Guard. */
public class GuardView extends AbstractObjectView<Guard> implements ObjectView<Guard> {

    private final JLabel name;
    private final JLabel surname;
    private final JLabel birthDate;
    private final JLabel rank;
    private final JLabel telephone;

    /**
     * Constructor.
     *
     * @param rank       the rank of the guard
     * @param parent     the parent view
     * @param controller the controller of this view
     */
    public GuardView(final int rank, final PrisonView parent, final ObjectViewController<Guard> controller) {
        super(rank, parent, "ID Guardia", controller);

        this.name = addTextView("Nome:");
        this.surname = addTextView("Cognome:");
        this.birthDate = addDateView("Data di nascita:", Person.DATE_FORMAT);
        this.rank = addTextView("Grado:");
        this.telephone = addTextView("Numero di telefono:");

        visible();
    }

    @Override
    public void parseObject(final Guard guard) {
        this.name.setText(guard.getName());
        this.surname.setText(guard.getSurname());
        this.telephone.setText(guard.getTelephoneNumber());
        this.rank.setText(String.valueOf(guard.getRank()));
        this.birthDate.setText(guard.getBirthDate().format(Person.DATE_TIME_FORMATTER));
    }
}
