package view.gui.view.single;

import controller.view.ObjectViewController;
import view.ObjectView;
import view.PrisonView;
import view.gui.GridPrisonView;

import javax.annotation.Nullable;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.util.Optional;

/**
 * The class models an abstract implementation of a view in Swing that shows a generic object.
 *
 * @param <T> {@inheritDoc}
 */
public abstract class AbstractObjectView<T> extends GridPrisonView implements ObjectView<T>, PrisonView {

    private final JTextField id;


    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param title      the title of the view
     * @param controller the controller of the view
     */
    public AbstractObjectView(final int rank, final @Nullable PrisonView parent, final String title, final ObjectViewController<T> controller) {
        super(rank, parent);

        this.id = new JTextField(2);
        final JLabel titleLabel = new JLabel(title);
        titleLabel.setLabelFor(this.id);
        addToNorth(titleLabel, this.id);

        final JButton view = new JButton("Vedi");
        view.addActionListener(e -> {
            final String id = getID().getText().trim();
            if (id.isEmpty()) {
                displayErrorMessage(title, "Devi inserire un ID");
            } else {
                Optional<T> object = controller.search(Integer.parseInt(id));
                if (object.isPresent()) {
                    parseObject(object.get());
                    visible();
                } else {
                    displayErrorMessage(title, "Non trovato");
                }
            }
        });
        addButtonsToSouth(view);
    }

    @Override
    public abstract void parseObject(final T object);

    /**
     * Get the {@code TextField} that contains the ID of the object represented.
     *
     * @return the field component
     */
    protected final JTextField getID() {
        return this.id;
    }
}
