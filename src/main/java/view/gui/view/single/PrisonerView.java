package view.gui.view.single;

import controller.view.ObjectViewController;
import model.Crime;
import model.people.Person;
import model.people.Prisoner;
import view.PrisonView;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;

/** View that represents a Prisoner. */
public class PrisonerView extends AbstractObjectView<Prisoner> {

    private final JLabel name;
    private final JLabel surname;
    private final JLabel birthDate;
    private final JLabel start;
    private final JLabel end;
    private final DefaultListModel<Crime> crimes;

    /**
     * Constructor.
     *
     * @param rank       the rank of the guard
     * @param parent     the parent view
     * @param controller the controller of this view
     */
    public PrisonerView(final int rank, final PrisonView parent, final ObjectViewController<Prisoner> controller) {
        super(rank, parent, "ID Prigioniero", controller);

        this.name = addTextView("Nome:");
        this.surname = addTextView("Cognome:");
        this.birthDate = addDateView("Data di nascita:", Person.DATE_FORMAT);
        this.start = addDateView("Inizio reclusione:", Person.DATE_FORMAT);
        this.end = addDateView("Fine reclusione:", Person.DATE_FORMAT);
        this.crimes = (DefaultListModel<Crime>) addList("Crimini:").getModel();

        visible();
    }

    @Override
    public void parseObject(final Prisoner prisoner) {
        this.name.setText(prisoner.getName());
        this.surname.setText(prisoner.getSurname());
        this.birthDate.setText(prisoner.getBirthDate().format(Person.DATE_TIME_FORMATTER));
        this.start.setText(prisoner.getStart().format(Person.DATE_TIME_FORMATTER));
        this.end.setText(prisoner.getFinish().format(Person.DATE_TIME_FORMATTER));
        this.crimes.removeAllElements();
        prisoner.getCrimes().forEach(this.crimes::addElement);
    }
}
