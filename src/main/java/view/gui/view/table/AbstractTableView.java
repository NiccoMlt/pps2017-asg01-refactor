package view.gui.view.table;

import view.PrisonView;
import view.TableView;
import view.gui.AbstractPrisonView;
import view.gui.components.PrisonPanel;

import javax.annotation.Nullable;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Collection;
import java.util.List;

/**
 * Abstract implementation of a {@code TableView} aimed to implement a view that parses {@link Collection} of raw data
 * to a {@link TableModel Swing TableModel}, that can be used by some components like {@link JTable Swing JTable}.
 *
 * @param <T> {@inheritDoc}
 */
public abstract class AbstractTableView<T> extends AbstractPrisonView implements TableView<T>, PrisonView {

    private final JTable table;
    private final JPanel north;
    private TableModel tableModel;
    private int northRowCount = 0;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param title      the title of the view
     */
    public AbstractTableView(final int rank, final @Nullable PrisonView parent, final String title) {
        super(rank, parent);

        north = new PrisonPanel(new GridBagLayout());
        final JLabel titleLabel = new JLabel(title);
        addRowToNorth(titleLabel);
        addToNorth(north);

        this.tableModel = new DefaultTableModel();
        table = new JTable(getTableModel());
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.setFillsViewportHeight(true);
        final JScrollPane js = new JScrollPane(table);
        js.setVisible(true);
        getFrame().pack();

        addToCenter(js);
    }

    @Override
    public void parseData(final Collection<T> data) {
        final DefaultTableModel model = new DefaultTableModel(getColumnNames().toArray(new String[0]), 0);
        data.stream()
                .map(this::parseObject)
                .map(List::toArray)
                .forEach(model::addRow);
        this.tableModel = model;
    }

    /**
     * Return the ordered list of column names.
     * <p>
     * The order and the number of elements should be the same used to {@link #parseObject(Object) parse objects}.
     *
     * @return the names of the columns
     */
    protected abstract List<String> getColumnNames();

    /**
     * Parse the given object to an ordered list of strings.
     * <p>
     * The order and the number of elements should be the same of the {@link #getColumnNames() column names}.
     *
     * @param object the object to parse
     * @return the representation of the object as a list of strings, one for each column
     */
    protected abstract List<String> parseObject(final T object);

    /**
     * Return current parsed {@link TableModel}, or a default empty one if no data was parsed.
     *
     * @return the model of the table
     */
    protected final TableModel getTableModel() {
        return this.tableModel;
    }

    @Override
    public final void updateTable() {
        this.table.setModel(getTableModel());
        getFrame().pack();
    }

    /**
     * Add a new row on the north panel with all the given components.
     *
     * @param components the components to add to a single row
     */
    protected void addRowToNorth(final JComponent... components) {
        int column = 0;

        for (final JComponent component : components) {
            final GridBagConstraints constraints = new GridBagConstraints();
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.gridy = northRowCount;
            constraints.gridx = column++;

            north.add(component, constraints);
        }
        northRowCount++;
    }
}
