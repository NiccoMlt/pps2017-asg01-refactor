package view.gui.view.table;

import controller.view.TableViewController;
import model.people.Person;
import model.people.Visitor;
import view.PrisonView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** View that represents all the Visitors. */
public class VisitorsView extends AbstractTableView<Visitor> {
    private static final String[] COLUMNS = {"Nome", "Cognome", "Data di nascita", "ID prigioniero visitato"};

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public VisitorsView(final int rank, final PrisonView parent, final TableViewController<Visitor> controller) {
        super(rank, parent, "Vedi visitatori");

        parseData(controller.get());
        updateTable();

        visible();
    }

    @Override
    protected List<String> getColumnNames() {
        return Arrays.asList(COLUMNS);
    }

    @Override
    protected List<String> parseObject(final Visitor visitor) {
        final List<String> list = new ArrayList<>();
        list.add(visitor.getName());
        list.add(visitor.getSurname());
        list.add(visitor.getBirthDate().format(Person.DATE_TIME_FORMATTER));
        list.add(String.valueOf(visitor.getPrisonerID()));
        return list;
    }
}
