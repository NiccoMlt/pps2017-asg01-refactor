package view.gui.view.table;

import controller.view.TableViewController;
import model.Cell;
import view.PrisonView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/** View that represents all the Cells of the prison. */
public class CellsView extends AbstractTableView<Cell> {

    private static final String[] COLUMNS = {"ID Cella", "Descrizione", "Prigionieri correnti", "Capacità max"};

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public CellsView(final int rank, final PrisonView parent, final TableViewController<Cell> controller) {
        super(rank, parent, "Vedi celle");

        parseData(controller.get());
        updateTable();

        visible();
    }

    @Override
    protected List<String> getColumnNames() {
        return Arrays.asList(COLUMNS);
    }

    @Override
    protected List<String> parseObject(final Cell cell) {
        final List<String> list = new ArrayList<>();
        list.add(String.valueOf(cell.getId()));
        list.add(cell.getDescription());
        list.add(String.valueOf(cell.getCurrentPrisoners()));
        list.add(String.valueOf(cell.getCapacity()));
        return list;
    }
}
