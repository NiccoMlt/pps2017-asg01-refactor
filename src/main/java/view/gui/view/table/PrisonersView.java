package view.gui.view.table;

import controller.view.TableViewController;
import model.people.Prisoner;
import view.PrisonView;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.time.Month.JANUARY;
import static model.people.Person.DATE_FORMAT;
import static model.people.Person.DATE_TIME_FORMATTER;

/** View that represents all the Prisoners in a given range of dates. */
public class PrisonersView extends AbstractTableView<Prisoner> {
    private static final String[] COLUMNS = {
            "ID", "Nome", "Cognome", "Data di nascita", "Inizio prigionia", "Fine prigionia"
    };
    private static final LocalDate FIRST_DATE = LocalDate.of(1980, JANUARY, 31);
    private static final LocalDate LAST_DATE = LocalDate.of(2021, JANUARY, 31);

    private final JTextField from;
    private final JTextField to;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public PrisonersView(final int rank, final PrisonView parent, final TableViewController<Prisoner> controller) {
        super(rank, parent, "Vista prigionieri");

        this.from = new JFormattedTextField(DATE_TIME_FORMATTER);
        this.from.setColumns(FIELD_COLUMNS);
        this.from.setText(FIRST_DATE.format(DATE_TIME_FORMATTER));
        final JLabel fromLabel = new JLabel("Reclusi Da (" + DATE_FORMAT.toLocalizedPattern() + ")");
        fromLabel.setLabelFor(this.from);
        this.to = new JFormattedTextField(DATE_TIME_FORMATTER);
        this.to.setColumns(FIELD_COLUMNS);
        this.to.setText(LAST_DATE.format(DATE_TIME_FORMATTER));
        final JLabel toLabel = new JLabel("A (" + DATE_FORMAT.toLocalizedPattern() + ")");
        toLabel.setLabelFor(this.to);
        addRowToNorth(fromLabel, this.from, toLabel, this.to);

        final JButton compute = new JButton("Calcola");
        compute.addActionListener(e -> {
            parseData(controller.get());
            updateTable();
        });
        addButtonsToSouth(compute);

        parseData(controller.get());
        updateTable();

        visible();
    }

    @Override
    protected List<String> getColumnNames() {
        return Arrays.asList(COLUMNS);
    }

    @Override
    protected List<String> parseObject(final Prisoner prisoner) {
        final List<String> list = new ArrayList<>();
        list.add(String.valueOf(prisoner.getPrisonerID()));
        list.add(prisoner.getName());
        list.add(prisoner.getSurname());
        list.add(prisoner.getBirthDate().format(DATE_TIME_FORMATTER));
        list.add(prisoner.getStart().format(DATE_TIME_FORMATTER));
        list.add(prisoner.getFinish().format(DATE_TIME_FORMATTER));
        return list;
    }

    @Override
    public void parseData(final Collection<Prisoner> data) {
        super.parseData(data
                .stream()
                .filter(p -> p.getStart().isBefore(getTo())
                        && p.getFinish().isAfter(getFrom()))
                .collect(Collectors.toList()));
    }

    /**
     * Get the date first date to look from.
     *
     * @return the date
     */
    private LocalDate getFrom() {
        return LocalDate.parse(from.getText(), DATE_TIME_FORMATTER);
    }

    /**
     * Get the date other date to look to.
     *
     * @return the date
     */
    private LocalDate getTo() {
        return LocalDate.parse(to.getText(), DATE_TIME_FORMATTER);
    }
}
