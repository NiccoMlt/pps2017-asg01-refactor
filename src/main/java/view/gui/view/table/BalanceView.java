package view.gui.view.table;

import controller.view.TableViewController;
import model.Movement;
import view.PrisonView;

import javax.swing.JLabel;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/** View that represents all the movements of the balance, and the current balance. */
public class BalanceView extends AbstractTableView<Movement> {
    private static final String[] COLUMNS = {"Ammontare", "Descrizione", "Data"};
    private static final String BALANCE_LABEL = "Bilancio";
    private static final String SEPARATOR = ": ";
    private final JLabel balanceLabel;
    private BigDecimal balance;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public BalanceView(final int rank, final PrisonView parent, final TableViewController<Movement> controller) {
        super(rank, parent, BALANCE_LABEL);

        balance = BigDecimal.ZERO;

        balanceLabel = new JLabel(SEPARATOR + balance.toPlainString());
        addToNorth(balanceLabel);

        parseData(controller.get());
        updateTable();

        visible();
    }

    @Override
    public void parseData(final Collection<Movement> data) {
        balance = BigDecimal.ZERO;
        super.parseData(data);
    }

    @Override
    protected List<String> getColumnNames() {
        return Arrays.asList(COLUMNS);
    }

    @Override
    protected List<String> parseObject(final Movement movement) {
        final List<String> list = new ArrayList<>();
        final BigDecimal amount = movement.getAmount();
        list.add(amount.toPlainString());
        addToBalance(amount);
        list.add(movement.getDescription());
        list.add(movement.getDate().format(Movement.DATE_TIME_FORMATTER));
        return list;
    }

    /**
     * Add an amount to the balance.
     *
     * @param amount the amount to add
     */
    private void addToBalance(final BigDecimal amount) {
        balance = balance.add(amount);
        balanceLabel.setText(SEPARATOR + balance.toPlainString());
    }
}
