package view.gui.components;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.LayoutManager;

/** Default {@code JPanel} for this application. */
public class PrisonPanel extends JPanel {

    private static final long serialVersionUID = 1L;
    private static final Color BG = new Color(210, 210, 210);

    /** Constructor. */
    public PrisonPanel() {
        super();
        this.setBackground(BG);
    }

    /**
     * Constructor with layout.
     *
     * @param layout The layout of the panel.
     */
    public PrisonPanel(final LayoutManager layout) {
        super(layout);
        this.setBackground(BG);
    }
}
