package view.gui.components;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RefineryUtilities;

import javax.swing.JFrame;
import java.util.Map;

/** Simple {@link JFrame Swing Window} containing a BarChart that shows the statistics of crimes per prisoner. */
public class BarChart extends JFrame {

    private static final long serialVersionUID = 1L;

    private final Map<Integer, Integer> map;

    /**
     * Constructor.
     *
     * @param map       the data the chart will represent
     * @param title     the title of the chart
     * @param chartName the name of the chart
     */
    public BarChart(final Map<Integer, Integer> map, final String title, final String chartName) {
        super(title);
        this.map = map;
        final JFreeChart barChart = ChartFactory.createBarChart(
                chartName,
                "Anno",
                "Numero di prigonieri",
                createDataset(),
                PlotOrientation.VERTICAL,
                true, true, false);
        final ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
        setContentPane(chartPanel);
        this.pack();
        RefineryUtilities.centerFrameOnScreen(this);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }

    /**
     * Creates the data set for the chart.
     *
     * @return the data set for the chart
     */
    private CategoryDataset createDataset() {
        final String name = "-";

        final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (Map.Entry<Integer, Integer> e : map.entrySet()) {
            dataset.addValue(Double.valueOf(e.getValue()), e.getKey(), name);
        }
        return dataset;
    }
}