package view.gui.components;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.RefineryUtilities;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.util.Map;

/** Simple {@link JFrame Swing Window} containing a PieChart that shows the statistics of prisoners per year. */
public class PieChart extends JFrame {

    private static final long serialVersionUID = 1L;

    private final Map<String, Integer> map;

    /**
     * Constructor.
     *
     * @param map   the data the chart will represent
     * @param title the title of the chart
     */
    public PieChart(final String title, final Map<String, Integer> map) {
        super(title);
        this.map = map;
        setContentPane(createPanel());
        this.setSize(600, 600);
        RefineryUtilities.centerFrameOnScreen(this);
        this.setVisible(true);
    }

    /**
     * Creates the data set for the chart.
     *
     * @return the data set for the chart
     */
    private PieDataset createDataset() {
        final DefaultPieDataset dataset = new DefaultPieDataset();
        for (final Map.Entry<String, Integer> e : this.map.entrySet()) {
            dataset.setValue(String.valueOf(e.getKey()), e.getValue());
        }
        return dataset;
    }

    /**
     * Creates the chart.
     *
     * @param dataset the data set the chart will use
     * @return the chart
     */
    private JFreeChart createChart(final PieDataset dataset) {
        return ChartFactory.createPieChart(
                "Percentuale crimini commessi dai reclusi attuali",  // chart title
                dataset,        // data
                true,           // include legend
                true,
                false);
    }

    /**
     * Creates the panel for the chart.
     *
     * @return the panel
     */
    private JPanel createPanel() {
        return new ChartPanel(createChart(createDataset()));
    }
}
