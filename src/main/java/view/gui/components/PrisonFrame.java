package view.gui.components;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import java.awt.Dimension;

/** Default {@code JFrame} for this application. */
public class PrisonFrame extends JFrame {

    private static final long serialVersionUID = 1L;
    private static final String LOGO_PATH = "/logo.png";
    private static final ImageIcon ICON = new ImageIcon(PrisonFrame.class.getResource(LOGO_PATH));

    /** Constructor. */
    public PrisonFrame() {
        super();
        this.setIconImage(getIcon().getImage());
        setResizable(false);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    /**
     * Constructor.
     * <p>
     * It specifies the {@link JFrame#setPreferredSize(Dimension) preferred size} of the frame.
     *
     * @param width  the preferred width of the frame
     * @param height the preferred height of the frame
     */
    public PrisonFrame(final int width, final int height) {
        this();
        this.setPreferredSize(new Dimension(width, height));
    }

    /**
     * Return the Icon of the application, used by default by this frame.
     *
     * @return the icon, loaded from resources
     */
    private static ImageIcon getIcon() {
        return ICON;
    }
}
