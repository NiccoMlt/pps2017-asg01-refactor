package view.gui;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.PrisonView;
import view.gui.components.PrisonFrame;
import view.gui.components.PrisonPanel;

import javax.annotation.Nullable;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.Arrays;
import java.util.Optional;

/**
 * Abstract implementation of a View for this application using on {@link JFrame Swing JFrame}.
 *
 * @see PrisonFrame
 */
public abstract class AbstractPrisonView implements PrisonView {

    /** Text filed preferred dimension in columns. */
    protected static final int FIELD_COLUMNS = 8;
    private static final Logger L = LoggerFactory.getLogger(PrisonView.class);
    private final int rank;
    private final PrisonView parent;
    private final JFrame frame;
    private final JPanel center;
    private final JPanel south;
    private final JPanel north;

    /**
     * Constructor.
     *
     * @param rank   the rank of the user
     * @param parent the parent view
     */
    public AbstractPrisonView(final int rank, final @Nullable PrisonView parent) {
        this.rank = rank;
        this.parent = parent;
        frame = new PrisonFrame();
        this.getFrame().getContentPane().setLayout(new BorderLayout());

        north = new PrisonPanel(new FlowLayout());
        this.getFrame().getContentPane().add(BorderLayout.NORTH, north);

        center = new PrisonPanel(new FlowLayout());
        this.getFrame().getContentPane().add(BorderLayout.CENTER, center);

        south = new PrisonPanel(new FlowLayout());
        if (getParent().isPresent()) {
            final JButton back = new JButton();
            back.setText("Indietro");
            back.addActionListener(ev -> {
                this.close();
                getParent().orElseThrow(IllegalStateException::new).visible();
            });
            addButtonsToSouth(back);
        }
        this.getFrame().getContentPane().add(BorderLayout.SOUTH, south);
    }

    /**
     * Return the {@code JFrame} used.
     *
     * @return the {@code JFrame}
     */
    protected JFrame getFrame() {
        return frame;
    }

    @Override
    public int getRank() {
        return rank;
    }

    @Override
    public final void visible() {
        this.frame.pack();
        this.frame.setLocationRelativeTo(null);
        this.frame.setVisible(true);
    }

    @Override
    public final void close() {
        this.frame.dispose();
    }

    /**
     * Get the parent view, if any.
     *
     * @return the parent view, if any
     */
    protected final Optional<PrisonView> getParent() {
        return Optional.ofNullable(this.parent);
    }

    /**
     * Add some components to the north of the frame.
     *
     * @param components the components to add
     */
    protected final void addToNorth(final JComponent... components) {
        Arrays.stream(components).forEach(north::add);
    }

    /**
     * Add some components to the center of the frame.
     *
     * @param components the components to add
     */
    protected final void addToCenter(final JComponent... components) {
        Arrays.stream(components).forEach(center::add);
    }

    /**
     * Add some buttons to the south of the frame.
     *
     * @param buttons the buttons to add
     */
    protected final void addButtonsToSouth(final JButton... buttons) {
        Arrays.stream(buttons).forEach(south::add);
    }

    /**
     * {@inheritDoc}
     *
     * @param title   {@inheritDoc}
     * @param message {@inheritDoc}
     * @see Logger#debug(String)
     * @see JOptionPane#INFORMATION_MESSAGE
     */
    @Override
    public final void displayInfoMessage(final String title, String message) {
        displayMessage(title, message, JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * {@inheritDoc}
     *
     * @param title   {@inheritDoc}
     * @param message {@inheritDoc}
     * @see Logger#error(String)
     * @see JOptionPane#ERROR_MESSAGE
     */
    @Override
    public final void displayErrorMessage(final String title, final String message) {
        displayMessage(title, message, JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Display message as {@link JOptionPane}, and log it.
     *
     * @param title   the title of the message
     * @param message the content of the message
     * @param type    the type of the message
     * @see JOptionPane
     * @see Logger
     * @see #displayInfoMessage(String, String)
     * @see #displayErrorMessage(String, String)
     */
    private void displayMessage(final String title, final String message, final int type) {
        JOptionPane.showMessageDialog(getFrame(), message, title, type);
        final String toPrint = title + ": " + message;
        switch (type) {
            case JOptionPane.ERROR_MESSAGE:
                L.error(toPrint);
                break;
            case JOptionPane.WARNING_MESSAGE:
                L.warn(toPrint);
                break;
            case JOptionPane.INFORMATION_MESSAGE:
                L.info(toPrint);
                break;
            default:
                L.debug(toPrint);
                break;
        }
    }
}
