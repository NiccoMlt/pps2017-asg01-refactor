package view.gui.insertion;

import controller.view.InsertionController;
import model.Movement;
import model.MovementImpl;
import view.PrisonView;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.math.BigDecimal;

/** View that adds a movement to the balance. */
public class AddMovementView extends AbstractInsertionView<Movement> {

    private final JTextField input;
    private final JTextField description;
    private final JComboBox<Character> values;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public AddMovementView(final int rank, final PrisonView parent, final InsertionController<Movement> controller) {
        super(rank, parent, "Aggiungi un movimento", controller);

        final Character[] signs = {'+', '-'};
        final JLabel label = new JLabel("+ : -");
        values = new JComboBox<>(signs);
        values.setSelectedIndex(0);
        label.setLabelFor(values);
        addRowToCenter(label, values);
        input = addTextField("Ammontare", FIELD_COLUMNS);
        description = addTextField("Descrizione", FIELD_COLUMNS);

        visible();
    }

    @Override
    public Movement get() {
        return new MovementImpl(description.getText(),
                new BigDecimal(values.getSelectedItem() + input.getText().trim()));
    }
}
