package view.gui.insertion;

import controller.view.InsertionController;
import view.ObjectInsertionView;
import view.PrisonView;
import view.gui.GridPrisonView;

import javax.annotation.Nullable;
import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * The class models an abstract implementation of a view in Swing that inserts a generic object.
 *
 * @param <T> {@inheritDoc}
 */
public abstract class AbstractInsertionView<T> extends GridPrisonView implements ObjectInsertionView<T> {

    private final String title;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param title      the title of the view
     * @param controller the controller of the view
     */
    public AbstractInsertionView(final int rank, final @Nullable PrisonView parent, final String title,
                                 final InsertionController<T> controller) {
        super(rank, parent);
        this.title = title;

        final JLabel titleLabel = new JLabel(title);
        addToNorth(titleLabel);

        final JButton insert = new JButton("Inserisci");
        insert.addActionListener(e -> insert(controller));
        addButtonsToSouth(insert);
    }

    /**
     * Insert the object.
     * <p>
     * It parses current options selected by the user and saves the object with the help of the controller.
     *
     * @param controller the controller that does the insertion
     */
    protected void insert(final InsertionController<T> controller) {
        if (controller.insert(get())) {
            displayInfoMessage(title, "Inserimento completato");
        } else {
            displayErrorMessage(title, "Completa tutti i campi correttamente");
        }
    }

    @Override
    public abstract T get();
}
