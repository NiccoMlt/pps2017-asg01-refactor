package view.gui.insertion;

import controller.view.InsertionController;
import model.Crime;
import model.people.PersonBuilder;
import model.people.Prisoner;
import view.ObjectInsertionView;
import view.PrisonView;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.time.LocalDate;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import static model.people.Person.DATE_FORMAT;
import static model.people.Person.DATE_TIME_FORMATTER;

/** View that adds a prisoner. */
public class AddPrisonerView extends AbstractInsertionView<Prisoner> implements ObjectInsertionView<Prisoner> {

    private final JComboBox<Crime> crimeType;
    private final DefaultListModel<Crime> crimeListModel;
    private final JTextField prisonerID;
    private final JTextField prisonerName;
    private final JTextField prisonerSurname;
    private final JTextField birthDate;
    private final JTextField start;
    private final JTextField end;
    private final JTextField cell;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public AddPrisonerView(final int rank, final PrisonView parent, final InsertionController<Prisoner> controller) {
        super(rank, parent, "Inserisci un prigioniero", controller);
        controller.setView(this);

        crimeType = addComboBoxToEast(Crime.values());
        crimeListModel = (DefaultListModel<Crime>) addList("Crimini selezionati").getModel();

        final JButton add = new JButton("Aggiungi un crimine");
        add.addActionListener(ev -> {
            final Crime crime = getCurrentlySelectedCrime();
            final Set<Crime> crimes = getSelectedCrimes();

            if (crimes.add(crime)) {
                setSelectedCrimes(crimes);
            } else {
                displayErrorMessage("Selezione crimini", "Crimine già inserito");
            }
        });
        addToEast(add);

        prisonerID = addTextField("ID Prigioniero", FIELD_COLUMNS);
        prisonerID.setText("0");
        prisonerName = addTextField("Nome", FIELD_COLUMNS);
        prisonerSurname = addTextField("Cognome", FIELD_COLUMNS);
        birthDate = addDateField("Data di nascita", DATE_FORMAT);
        birthDate.setText(DEFAULT_BIRTH_DATE.format(DATE_TIME_FORMATTER));
        start = addDateField("Inizio incarcerazione", DATE_FORMAT);
        start.setText(DEFAULT_START_DATE.format(DATE_TIME_FORMATTER));
        end = addDateField("Fine incarcerazione", DATE_FORMAT);
        end.setText(DEFAULT_END_DATE.format(DATE_TIME_FORMATTER));
        cell = addTextField("ID Cella", FIELD_COLUMNS);
        cell.setText("0");

        visible();
    }

    /**
     * Add a {@link JComboBox} to the east panel.
     *
     * @param values the values of the ComboBox model
     * @param <T>    the generic type of the values in the model
     * @return the created ComboBox
     */
    private <T> JComboBox<T> addComboBoxToEast(final T[] values) {
        final JComboBox<T> comboBox = new JComboBox<>(values);
        comboBox.setSelectedIndex(0);
        addToEast(comboBox);
        return comboBox;
    }

    /**
     * Get all the selected crimes.
     *
     * @return the selected crimes
     */
    private Set<Crime> getSelectedCrimes() {
        if (crimeListModel.elements().hasMoreElements()) {
            return EnumSet.copyOf(Collections.list(crimeListModel.elements()));
        } else {
            return EnumSet.noneOf(Crime.class);
        }
    }

    /**
     * Set the currently selected crimes.
     *
     * @param crimes the crimes to select
     */
    private void setSelectedCrimes(final Set<Crime> crimes) {
        crimeListModel.removeAllElements();
        crimes.forEach(crimeListModel::addElement);
    }

    /**
     * Get the currently selected crime.
     *
     * @return the currently selected crime
     */
    private Crime getCurrentlySelectedCrime() {
        return (Crime) crimeType.getSelectedItem();
    }

    @Override
    public Prisoner get() {
        return new PersonBuilder(prisonerName.getText(), prisonerSurname.getText(),
                LocalDate.parse(birthDate.getText(), DATE_TIME_FORMATTER))
                .buildPrisoner(Integer.parseInt(prisonerID.getText()), Integer.parseInt(cell.getText()),
                        LocalDate.parse(start.getText(), DATE_TIME_FORMATTER),
                        LocalDate.parse(end.getText(), DATE_TIME_FORMATTER))
                .addCrimes(getSelectedCrimes())
                .build();
    }

    @Override
    protected void insert(final InsertionController<Prisoner> controller) {
        if (controller.insert(get())) {
            setSelectedCrimes(EnumSet.noneOf(Crime.class));
        }
    }
}
