package view.gui.insertion;

import controller.view.InsertionController;
import model.people.Guard;
import model.people.PersonBuilder;
import view.PrisonView;

import javax.swing.JTextField;
import java.time.LocalDate;

import static model.people.Guard.MIN_PASSWORD;
import static model.people.Person.DATE_FORMAT;
import static model.people.Person.DATE_TIME_FORMATTER;

/** View that adds a guard. */
public class AddGuardView extends AbstractInsertionView<Guard> {

    private final JTextField guardID;
    private final JTextField guardName;
    private final JTextField guardSurname;
    private final JTextField birthDate;
    private final JTextField guardRank;
    private final JTextField telephoneNum;
    private final JTextField password;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public AddGuardView(final int rank, final PrisonView parent, final InsertionController<Guard> controller) {
        super(rank, parent, "Inserisci una guardia", controller);

        this.guardID = addTextField("ID Guardia", 2);
        this.guardID.setText("0");
        this.guardName = addTextField("Nome", FIELD_COLUMNS);
        this.guardSurname = addTextField("Cognome", FIELD_COLUMNS);
        this.birthDate = addDateField("Data di nascita", DATE_FORMAT);
        this.birthDate.setText(DEFAULT_BIRTH_DATE.format(DATE_TIME_FORMATTER));
        this.telephoneNum = addTextField("Numero di telefono", FIELD_COLUMNS);
        this.guardRank = addTextField("Grado", FIELD_COLUMNS);
        this.guardRank.setText("0");
        this.password = addTextField("Password (" + MIN_PASSWORD + " caratt. min)", FIELD_COLUMNS);

        visible();
    }

    @Override
    public Guard get() {
        return new PersonBuilder(guardName.getText(), guardSurname.getText(),
                LocalDate.parse(birthDate.getText(), DATE_TIME_FORMATTER))
                .buildGuard(Integer.valueOf(guardRank.getText()), telephoneNum.getText(),
                        Integer.valueOf(guardID.getText()), password.getText())
                .build();
    }
}
