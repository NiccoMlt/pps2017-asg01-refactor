package view.gui.insertion;

import controller.view.InsertionController;
import model.people.Person;
import model.people.PersonBuilder;
import model.people.Visitor;
import view.PrisonView;
import view.gui.components.PrisonPanel;

import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridLayout;
import java.time.LocalDate;
import java.time.Month;

/** View that adds a visitor. */
public class AddVisitorView extends AbstractInsertionView<Visitor> {

    private final JTextField name;
    private final JTextField surname;
    private final JTextField birthDate;
    private final JTextField prisonerID;

    /**
     * Constructor.
     *
     * @param rank       the rank of the user
     * @param parent     the parent view
     * @param controller the controller of the view
     */
    public AddVisitorView(final int rank, final PrisonView parent, final InsertionController<Visitor> controller) {
        super(rank, parent, "Inserisci visitatore", controller);

        final PrisonPanel center = new PrisonPanel(new GridLayout(4, 2));
        this.name = new JTextField(FIELD_COLUMNS);
        final JLabel nameLabel = new JLabel("Nome : ");
        nameLabel.setLabelFor(name);
        center.add(nameLabel);
        center.add(this.name);
        this.surname = new JTextField(FIELD_COLUMNS);
        final JLabel surnameLabel = new JLabel("Cognome :");
        surnameLabel.setLabelFor(surname);
        center.add(surnameLabel);
        center.add(this.surname);
        this.birthDate = new JTextField(FIELD_COLUMNS);
        this.birthDate.setText(LocalDate.of(2017, Month.JANUARY, 1).format(Person.DATE_TIME_FORMATTER));
        final JLabel birthDateLabel = new JLabel("Data di nascita " + Person.DATE_FORMAT.toLocalizedPattern() + " : ");
        birthDateLabel.setLabelFor(birthDate);
        center.add(birthDateLabel);
        center.add(this.birthDate);
        this.prisonerID = new JTextField(FIELD_COLUMNS);
        this.prisonerID.setText("0");
        final JLabel prisonerIDLabel = new JLabel("Id prigioniero incontrato  : ");
        prisonerIDLabel.setLabelFor(prisonerID);
        center.add(prisonerIDLabel);
        center.add(this.prisonerID);
        addToCenter(center);

        visible();
    }

    @Override
    public Visitor get() {
        return new PersonBuilder(name.getText(), surname.getText(),
                LocalDate.parse(birthDate.getText(), Person.DATE_TIME_FORMATTER))
                .buildVisitor(Integer.valueOf(prisonerID.getText()))
                .build();
    }
}
