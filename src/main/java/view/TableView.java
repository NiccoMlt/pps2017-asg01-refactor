package view;

import java.util.Collection;

/**
 * The interface models a view that shows a collection of generic data as a table showing, for each object in a row,
 * some different properties in the columns.
 *
 * @param <T> the generic type of data
 */
public interface TableView<T> extends PrisonView {

    /**
     * Parse a collection of data to an internal table model.
     *
     * @param data a collection of model data
     */
    void parseData(Collection<T> data);

    /**
     * Show the table from an internal model.
     *
     * @see #parseData(Collection)
     */
    void updateTable();
}
