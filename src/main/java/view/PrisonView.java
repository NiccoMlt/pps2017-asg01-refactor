package view;

/** The interface models a View of this application. */
public interface PrisonView {

    /**
     * Get the rank, that defines the access permission level of the user.
     *
     * @return the rank
     */
    int getRank();

    /** Show the UI. */
    void visible();

    /** Close the UI. */
    void close();

    /**
     * Display a message of information to the user.
     * <p>
     * Could be an {@code stdout} log, or a communication dialog.
     *
     * @param title   the title of the message
     * @param message the content of the message
     */
    void displayInfoMessage(String title, String message);

    /**
     * Display a message of error to the user.
     * <p>
     * Could be an {@code stderr} log, or an error dialog.
     *
     * @param title   the title of the message
     * @param message the content of the message
     */
    void displayErrorMessage(String title, String message);
}
