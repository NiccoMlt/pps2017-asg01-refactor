package view;

/**
 * The interface models a view that represents an object of the model.
 *
 * @param <T> the type of object
 */
public interface ObjectView<T> extends PrisonView {

    /**
     * Parse the model object as a representation in the view.
     *
     * @param object the object to represent
     */
    void parseObject(T object);
}
