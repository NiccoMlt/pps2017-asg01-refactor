package view;

/**
 * The interface models a view that adds an object of the model.
 *
 * @param <T> the type of object
 */
public interface ObjectInsertionView<T> extends PrisonView {

    /**
     * Gets the object to add.
     *
     * @return the object
     */
    T get();
}
