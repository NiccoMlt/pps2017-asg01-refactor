package view;

/**
 * The interface models a View for the login process.
 */
public interface Login extends PrisonView {

    /**
     * Get the username as String.
     *
     * @return the username
     */
    String getUsername();

    /**
     * Get the password as String.
     *
     * @return the password
     */
    String getPassword();
}
