package view;

/**
 * The interface models a view that removes an object of the model.
 *
 * @param <T> the type of object
 */
@SuppressWarnings("unused") // The generic type is used to better identify the views
public interface ObjectRemovalView<T> extends PrisonView {

    /**
     * Gets the identifier of the object to remove.
     *
     * @return the unique ID
     */
    int getID();
}
