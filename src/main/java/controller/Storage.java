package controller;

import model.Cell;
import model.Movement;
import model.people.Guard;
import model.people.Prisoner;
import model.people.Visitor;

import java.util.List;

/** Interface that models a storage object for the application. */
public interface Storage {

    /**
     * Get the currently imprisoned prisoners.
     *
     * @return the list of prisoners
     */
    List<Prisoner> getCurrentPrisoners();

    /**
     * Set the currently imprisoned prisoners.
     *
     * @param prisoners the list of prisoners
     */
    void setCurrentPrisoners(List<Prisoner> prisoners);

    /**
     * Get all the prisoners.
     *
     * @return the list of prisoners
     */
    List<Prisoner> getPrisoners();

    /**
     * Set all the prisoners.
     *
     * @param prisoners the list of prisoners
     */
    void setPrisoners(List<Prisoner> prisoners);

    /**
     * Get the visitors.
     *
     * @return the list of visitors
     */
    List<Visitor> getVisitors();

    /**
     * Set the visitors.
     *
     * @param visitors the visitors
     */
    void setVisitors(List<Visitor> visitors);

    /**
     * Get the guards.
     *
     * @return the list of guards
     */
    List<Guard> getGuards();

    /**
     * Set the guards.
     *
     * @param guards the list of guards
     */
    void setGuards(List<Guard> guards);

    /**
     * Get the cells.
     *
     * @return the list of cells
     */
    List<Cell> getCells();

    /**
     * Set the cells.
     *
     * @param cells the list of cells
     */
    void setCells(List<Cell> cells);

    /**
     * Get the movements of the balance.
     *
     * @return the list of movements
     */
    List<Movement> getMovements();

    /**
     * Set the movements of the balance.
     *
     * @param movements the list of movements
     */
    void setMovements(List<Movement> movements);

    /**
     * Add a prisoner only to the current prisoners.
     *
     * @param prisoner the prisoner
     */
    void addCurrentPrisoner(Prisoner prisoner);

    /**
     * Add a prisoner to the prisoners.
     *
     * @param prisoner the prisoner
     */
    void addPrisoner(Prisoner prisoner);

    /**
     * Add a visitor.
     *
     * @param visitor the visitor
     */
    void addVisitor(Visitor visitor);

    /**
     * Add a guard to the system.
     *
     * @param guard the guard
     */
    void addGuard(Guard guard);

    /**
     * Add a prisoner to his/her cell.
     *
     * @param prisoner the prisoner
     */
    void addToCell(Prisoner prisoner);

    /**
     * Remove a prisoner from his/her cell.
     *
     * @param prisoner the prisoner
     */
    void removeFromCell(Prisoner prisoner);

    /**
     * Add a movement to the balance.
     *
     * @param movement the movement
     */
    void addMovement(Movement movement);
}
