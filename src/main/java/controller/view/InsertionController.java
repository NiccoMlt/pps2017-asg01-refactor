package controller.view;

import controller.Storage;
import view.ObjectInsertionView;

/**
 * Interface of a controller for a View that adds a model object to the {@link Storage}.
 *
 * @param <T> {@inheritDoc}
 */
public interface InsertionController<T> extends ViewController<ObjectInsertionView<T>> {

    /**
     * Insert the object in the {@link Storage}.
     *
     * @param object the object to add
     * @return true if the insertion gone right
     */
    boolean insert(T object);
}
