package controller.view;

import view.PrisonView;

/**
 * The interface models a ViewController for a View of this application.
 * <p>
 * The ViewController can or can not be used without specifying a View with {@link #setView(PrisonView) setView()},
 * but should support it.
 *
 * @param <V> the type of the view
 */
public interface ViewController<V extends PrisonView> {

    /**
     * Set a View the controller can model the behavior and handle the interaction with the Model for.
     *
     * @param view the View object
     */
    void setView(V view);
}
