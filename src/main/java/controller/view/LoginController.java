package controller.view;

import model.people.Guard;
import view.Login;

/** ViewController interface that implements a controller for a Login View. */
public interface LoginController extends ViewController<Login> {

    /**
     * Try to log the user in; if credential are correct, switch to another View.
     *
     * @param username the username
     * @param password the password
     */
    void logUserIn(String username, String password);

    /**
     * Test the credentials to get the rank of the specified user.
     * <p>
     * If not possible, it return {@value Guard#NO_RANK}.
     *
     * @param username the username
     * @param password the password
     * @return the rank, if credentials are valid, or {@value Guard#NO_RANK}
     * @see Guard#NO_RANK
     */
    int testCredentialsFor(String username, String password);
}
