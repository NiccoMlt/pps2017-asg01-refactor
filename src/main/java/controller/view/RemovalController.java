package controller.view;

import controller.Storage;
import view.ObjectRemovalView;

/**
 * Interface of a controller for a View that removes a model object from the {@link Storage}.
 *
 * @param <T> {@inheritDoc}
 */
public interface RemovalController<T> extends ViewController<ObjectRemovalView<T>> {

    /**
     * Remove the object with the give identifier from the {@link Storage}.
     *
     * @param id the ID of the object to remove
     * @return true if the removal gone right
     */
    boolean remove(int id);
}
