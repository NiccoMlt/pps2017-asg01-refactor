package controller.view.gui;

import controller.Storage;
import controller.view.ViewController;
import model.people.Guard;
import view.PrisonView;

import java.util.Optional;

/**
 * This class models an abstract implementation of a controller class for a generic View.
 *
 * @param <V> {@inheritDoc}
 */
public abstract class AbstractGuiController<V extends PrisonView> implements ViewController<V> {
    private final Storage storage;
    private V view;

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public AbstractGuiController(final Storage storage) {
        this.storage = storage;
    }

    /**
     * Get the view controlled by this class, if any.
     *
     * @return the controlled view, if any
     */
    protected final Optional<V> getView() {
        return Optional.ofNullable(view);
    }

    @Override
    public final void setView(final V view) {
        this.view = view;
    }

    /**
     * Get the rank the user is using the view with.
     *
     * @return the rank of the user
     * @see PrisonView#getRank()
     */
    protected int getRank() {
        return getView().map(PrisonView::getRank).orElse(Guard.NO_RANK);
    }

    /**
     * Get the internal storage object.
     *
     * @return the storage object
     */
    protected final Storage getStorage() {
        return this.storage;
    }
}
