package controller.view.gui;

import controller.Storage;
import controller.view.InsertionController;
import controller.view.MenuController;
import controller.view.ObjectViewController;
import controller.view.RemovalController;
import controller.view.TableViewController;
import controller.view.gui.insertion.AddGuardController;
import controller.view.gui.insertion.AddMovementController;
import controller.view.gui.insertion.AddPrisonerController;
import controller.view.gui.insertion.AddVisitorController;
import controller.view.gui.removal.RemoveGuardController;
import controller.view.gui.removal.RemovePrisonerController;
import controller.view.gui.view.BalanceViewController;
import controller.view.gui.view.CellsViewController;
import controller.view.gui.view.GuardViewController;
import controller.view.gui.view.PrisonerViewController;
import controller.view.gui.view.PrisonersViewController;
import controller.view.gui.view.VisitorsViewController;
import javafx.util.Pair;
import model.Cell;
import model.Crime;
import model.Movement;
import model.people.Guard;
import model.people.Prisoner;
import model.people.Visitor;
import view.PrisonView;
import view.gui.components.BarChart;
import view.gui.components.PieChart;

import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

/** Controller class for a View that shows a menu. */
public class MenuControllerImpl extends AbstractGuiController<PrisonView> implements MenuController {

    private static final int OPENING_YEAR = 2017;

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public MenuControllerImpl(final Storage storage) {
        super(storage);
    }

    @Override
    public InsertionController<Prisoner> addPrisoner() {
        return new AddPrisonerController(getStorage());
    }

    @Override
    public RemovalController<Prisoner> removePrisoner() {
        return new RemovePrisonerController(getStorage());
    }

    @Override
    public ObjectViewController<Prisoner> viewPrisoner() {
        return new PrisonerViewController(getStorage());
    }

    @Override
    public InsertionController<Movement> addMovement() {
        return new AddMovementController(getStorage());
    }

    @Override
    public TableViewController<Movement> viewBalance() {
        return new BalanceViewController(getStorage());
    }

    /**
     * Gets the bigger year of the finish dates of the prisoners.
     *
     * @param list the list of prisoners to search into
     * @return the max year
     * @see Prisoner#getFinish()
     */
    private int getMax(final List<Prisoner> list) {
        return list
                .stream()
                .map(Prisoner::getFinish)
                .max(LocalDate::compareTo)
                .map(LocalDate::getYear)
                .orElse(0);
    }

    @Override
    public void buildBarChart() {
        final List<Prisoner> prisoners = getStorage().getPrisoners();

        final Map<Integer, Integer> map = IntStream
                .rangeClosed(OPENING_YEAR, getMax(prisoners))
                .mapToObj(String::valueOf)
                .map(Year::parse)
                .map(y -> new Pair<>(y, prisoners
                        .stream()
                        .map(p -> new Pair<>(Year.from(p.getStart()), Year.from(p.getFinish())))
                        .filter(p -> p.getKey().isAfter(y) || p.getValue().isBefore(y))
                        .count()))
                .collect(Collectors.toMap(
                        yearLongPair -> Integer
                                .parseInt(DateTimeFormatter
                                        .ofPattern("yyyy")
                                        .format(yearLongPair
                                                .getKey())),
                        yearLongPair -> yearLongPair.getValue().intValue()
                ));

        final BarChart chart = new BarChart(map, "Numero prigionieri per anno", "Numero prigionieri per anno");
        chart.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    @Override
    public void buildPieChart() {
        final Map<String, Integer> crimeCounter = Arrays
                .stream(Crime.values())
                .map(Crime::getDescription)
                .collect(Collectors.toMap(crime -> crime, crime -> 0));

        getStorage().getCurrentPrisoners()
                .stream()
                .map(Prisoner::getCrimes)
                .flatMap(Set::stream)
                .map(Crime::getDescription)
                .forEach(crime -> crimeCounter.put(crime, crimeCounter.getOrDefault(crime, 0) + 1));

        final PieChart pie = new PieChart("Percentuale crimini commessi dai reclusi attuali", crimeCounter);
        pie.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    @Override
    public InsertionController<Visitor> addVisitor() {
        return new AddVisitorController(getStorage());
    }

    @Override
    public TableViewController<Visitor> viewVisitors() {
        return new VisitorsViewController(getStorage());
    }

    @Override
    public TableViewController<Cell> viewCells() {
        return new CellsViewController(getStorage());
    }

    @Override
    public InsertionController<Guard> addGuard() {
        return new AddGuardController(getStorage());
    }

    @Override
    public RemovalController<Guard> removeGuard() {
        return new RemoveGuardController(getStorage());
    }

    @Override
    public ObjectViewController<Guard> viewGuard() {
        return new GuardViewController(getStorage());
    }

    @Override
    public TableViewController<Prisoner> viewPrisoners() {
        return new PrisonersViewController(getStorage());
    }

}
