package controller.view.gui;

import controller.Storage;
import controller.serialization.SerializationStorage;
import controller.serialization.json.GsonSerializer;
import controller.view.LoginController;
import controller.view.MenuController;
import model.people.Guard;
import view.Login;
import view.PrisonView;
import view.gui.menu.MainView;

import java.util.Optional;

/** Controller class for a View that lets the user log into the application. */
public class LoginControllerImpl implements LoginController {
    private static final String LOGIN_TITLE = "Login";

    private final Storage storage;
    private Login view;

    /** Constructor. */
    public LoginControllerImpl() {
        storage = new SerializationStorage(new GsonSerializer());
    }

    @Override
    public void logUserIn(String username, String password) {
        if (username == null || username.trim().isEmpty()) {
            getView().ifPresent(v -> v.displayErrorMessage(LOGIN_TITLE, "L'username non può essere vuoto"));
        } else if (password == null || password.trim().isEmpty()) {
            getView().ifPresent(v -> v.displayErrorMessage(LOGIN_TITLE, "La password non può essere vuota"));
        } else {
            final Optional<Guard> guard = getGuard(username, password);
            if (guard.isPresent()) {
                getView().ifPresent(v -> {
                    v.displayInfoMessage(LOGIN_TITLE, "Benvenuto Utente " + username);
                    v.close();
                });
                final MenuController controller = new MenuControllerImpl(storage);
                controller.setView(new MainView(guard.orElseThrow(IllegalStateException::new).getRank(), controller));
                getView().ifPresent(PrisonView::close);
            } else {
                getView().ifPresent(v -> v.displayErrorMessage(LOGIN_TITLE, "Combinazione username/password errata"));
            }
        }
    }

    @Override
    public int testCredentialsFor(final String username, final String password) {
        return getGuard(username, password).map(Guard::getGuardID).orElse(Guard.NO_RANK);
    }

    /**
     * Get the guard with given credentials, if any.
     *
     * @param username the guard username
     * @param password the guard password
     * @return the matching guard, if any
     */
    private Optional<Guard> getGuard(final String username, final String password) {
        return storage
                .getGuards()
                .stream()
                .filter(g -> username.equals(String.valueOf(g.getGuardID())))
                .filter(g -> password.equals(g.getPassword()))
                .findFirst();
    }

    /**
     * Get the view object, if any.
     *
     * @return the view, if any
     */
    private Optional<Login> getView() {
        return Optional.ofNullable(this.view);
    }

    @Override
    public void setView(final Login view) {
        this.view = view;
    }
}
