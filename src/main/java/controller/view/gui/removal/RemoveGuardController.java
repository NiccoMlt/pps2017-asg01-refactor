package controller.view.gui.removal;

import controller.Storage;
import controller.view.RemovalController;
import controller.view.gui.AbstractGuiController;
import model.people.Guard;
import view.ObjectRemovalView;

import java.util.List;

/** The class implements a controller for a View used to remove a Guard. */
public class RemoveGuardController
        extends AbstractGuiController<ObjectRemovalView<Guard>> implements RemovalController<Guard> {

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public RemoveGuardController(final Storage storage) {
        super(storage);
    }

    @Override
    public boolean remove(int id) {
        final List<Guard> guards = getStorage().getGuards();

        if (guards.removeIf(g -> g.getGuardID() == id)) {
            getStorage().setGuards(guards);
            return true;
        }

        return false;
    }
}
