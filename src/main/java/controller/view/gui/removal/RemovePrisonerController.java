package controller.view.gui.removal;

import controller.Storage;
import controller.view.RemovalController;
import controller.view.gui.AbstractGuiController;
import model.people.Prisoner;
import view.ObjectRemovalView;

import java.util.Optional;

/** The class implements a controller for a View used to remove a Prisoner. */
public class RemovePrisonerController extends AbstractGuiController<ObjectRemovalView<Prisoner>> implements RemovalController<Prisoner> {

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public RemovePrisonerController(final Storage storage) {
        super(storage);
    }

    @Override
    public boolean remove(final int id) {
        final Optional<Prisoner> prisoner = getStorage().getCurrentPrisoners()
                .stream()
                .filter(p -> p.getPrisonerID() == id)
                .findFirst();

        if (prisoner.isPresent()) {
            getStorage().removeFromCell(prisoner.get());
            return true;
        }

        return false;
    }
}