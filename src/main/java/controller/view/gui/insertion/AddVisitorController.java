package controller.view.gui.insertion;

import controller.Storage;
import controller.view.InsertionController;
import controller.view.gui.AbstractGuiController;
import model.people.Prisoner;
import model.people.Visitor;
import view.ObjectInsertionView;

/** The class implements a controller for a View used to insert a Visitor. */
public class AddVisitorController
        extends AbstractGuiController<ObjectInsertionView<Visitor>> implements InsertionController<Visitor> {

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public AddVisitorController(final Storage storage) {
        super(storage);
    }

    @Override
    public boolean insert(final Visitor visitor) {
        if (checkPrisonerID(visitor.getPrisonerID())) {
            getStorage().addVisitor(visitor);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the prisoner ID is valid.
     *
     * @param prisonerID the ID to check
     * @return true if it's valid, false otherwise
     */
    private boolean checkPrisonerID(final int prisonerID) {
        return getStorage()
                .getPrisoners()
                .stream()
                .map(Prisoner::getPrisonerID)
                .noneMatch(id -> id == prisonerID);
    }
}



