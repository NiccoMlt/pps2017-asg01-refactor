package controller.view.gui.insertion;

import controller.Storage;
import controller.view.InsertionController;
import controller.view.gui.AbstractGuiController;
import model.Movement;
import view.ObjectInsertionView;

/** The class implements a controller for a View used to insert a Movement. */
public class AddMovementController
        extends AbstractGuiController<ObjectInsertionView<Movement>> implements InsertionController<Movement> {

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public AddMovementController(final Storage storage) {
        super(storage);
    }

    @Override
    public boolean insert(final Movement movement) {
        getStorage().addMovement(movement);
        return true;
    }

}
	
	
	

