package controller.view.gui.insertion;

import controller.Storage;
import controller.view.InsertionController;
import controller.view.gui.AbstractGuiController;
import model.people.Guard;
import view.ObjectInsertionView;

/** The class implements a controller for a View used to insert a Guard. */
public class AddGuardController
        extends AbstractGuiController<ObjectInsertionView<Guard>> implements InsertionController<Guard> {

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public AddGuardController(final Storage storage) {
        super(storage);
    }

    @Override
    public boolean insert(final Guard guard) {
        if (check(guard)) {
            getStorage().addGuard(guard);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if the guard is correctly implemented.
     *
     * @param g the guard to check
     * @return true if it's ok, false otherwise
     */
    private boolean check(final Guard g) {
        return getStorage()
                .getGuards()
                .stream()
                .map(Guard::getGuardID)
                .noneMatch(id -> g.getGuardID() == id)
                || g.getName().trim().equals("")
                || g.getSurname().trim().equals("")
                || g.getRank() < 1
                || g.getRank() > 3
                || g.getGuardID() < 0
                || g.getPassword().length() < 6;
    }
}
