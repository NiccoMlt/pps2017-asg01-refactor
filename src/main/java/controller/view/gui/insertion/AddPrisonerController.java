package controller.view.gui.insertion;

import controller.Storage;
import controller.view.InsertionController;
import controller.view.gui.AbstractGuiController;
import model.Cell;
import model.people.Prisoner;
import view.ObjectInsertionView;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


public class AddPrisonerController extends AbstractGuiController<ObjectInsertionView<Prisoner>> implements InsertionController<Prisoner> {
    private static final int MIN_CELL_ID = 0;
    private static final int MAX_CELL_ID = 49;
    private static final String TITLE = "Inserimento prigioniero";

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public AddPrisonerController(final Storage storage) {
        super(storage);
    }

    private boolean isSomethingEmpty(final Prisoner p) {
        return p.getName().isEmpty() || p.getSurname().isEmpty() || p.getCrimes().size() == 1;
    }

    @Override
    public boolean insert(final Prisoner prisoner) {
        final List<Prisoner> prisoners = getStorage().getPrisoners();

        if (isSomethingEmpty(prisoner)) {
            getView().ifPresent(v -> v.displayErrorMessage(TITLE, "Completa tutti i campi"));
        } else if (prisoners.stream().map(Prisoner::getPrisonerID).anyMatch(id -> id == prisoner.getPrisonerID())) {
            getView().ifPresent(v -> v.displayErrorMessage(TITLE, "ID già usato"));
        } else if (prisoner.getStart().isAfter(prisoner.getFinish()) || prisoner.getStart().isBefore(LocalDate.now())
                || prisoner.getBirthDate().isAfter(LocalDate.now())) {
            getView().ifPresent(v -> v.displayErrorMessage(TITLE, "Correggi le date"));
        } else if (prisoner.getCellID() < MIN_CELL_ID || prisoner.getCellID() > MAX_CELL_ID) {
            getView().ifPresent(v -> v.displayErrorMessage(TITLE, "Prova con un'altra cella"));
        } else {
            final Optional<Cell> cell = getStorage().getCells()
                    .stream()
                    .filter(c -> c.getId() == prisoner.getCellID())
                    .findFirst();

            if (!cell.isPresent() || cell.get().getCapacity() == cell.get().getCurrentPrisoners()) {
                getView().ifPresent(v -> v.displayErrorMessage(TITLE, "Prova con un'altra cella"));
            } else {
                getStorage().addToCell(prisoner);
                getView().ifPresent(v -> v.displayInfoMessage(TITLE, "Prigioniero inserito"));
                return true;
            }
        }
        return false;
    }
}
