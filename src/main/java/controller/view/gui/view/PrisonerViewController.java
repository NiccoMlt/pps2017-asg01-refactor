package controller.view.gui.view;

import controller.Storage;
import controller.view.ObjectViewController;
import controller.view.gui.AbstractGuiController;
import model.people.Prisoner;
import view.ObjectView;

import java.util.Optional;

/** Controller class for a view that graphically represents a prisoner. */
public class PrisonerViewController
        extends AbstractGuiController<ObjectView<Prisoner>> implements ObjectViewController<Prisoner> {

    /**
     * Constructor.
     *
     * @param storage the storage object to use
     */
    public PrisonerViewController(final Storage storage) {
        super(storage);
    }

    @Override
    public Optional<Prisoner> search(final int id) {
        return getStorage()
                .getPrisoners()
                .stream()
                .filter(p -> p.getPrisonerID() == id)
                .findFirst();
    }
}
