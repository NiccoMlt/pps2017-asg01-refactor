package controller.view.gui.view;

import controller.Storage;
import controller.view.TableViewController;
import controller.view.gui.AbstractGuiController;
import model.people.Visitor;
import view.TableView;

import java.util.Collection;

/** Controller class for a view that graphically represents all the Visitors in a table. */
public class VisitorsViewController
        extends AbstractGuiController<TableView<Visitor>> implements TableViewController<Visitor> {

    /**
     * Constructor.
     *
     * @param storage the storage object
     */
    public VisitorsViewController(final Storage storage) {
        super(storage);
    }

    @Override
    public Collection<Visitor> get() {
        return getStorage().getVisitors();
    }
}
