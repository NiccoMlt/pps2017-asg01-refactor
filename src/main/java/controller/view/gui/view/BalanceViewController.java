package controller.view.gui.view;

import controller.Storage;
import controller.view.TableViewController;
import controller.view.gui.AbstractGuiController;
import model.Movement;
import view.TableView;

import java.util.Collection;

/** Controller class for a view that graphically represents all the Movements of the balance in a table. */
public class BalanceViewController
        extends AbstractGuiController<TableView<Movement>> implements TableViewController<Movement> {

    /**
     * Constructor.
     *
     * @param storage the storage object to use
     */
    public BalanceViewController(final Storage storage) {
        super(storage);
    }

    @Override
    public Collection<Movement> get() {
        return getStorage().getMovements();
    }
}
