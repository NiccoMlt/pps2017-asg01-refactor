package controller.view.gui.view;

import controller.Storage;
import controller.view.ObjectViewController;
import controller.view.gui.AbstractGuiController;
import model.people.Guard;
import view.ObjectView;

import java.util.Optional;

/** Controller class for a view that graphically represents a guard. */
public class GuardViewController
        extends AbstractGuiController<ObjectView<Guard>> implements ObjectViewController<Guard> {

    /**
     * Constructor.
     *
     * @param storage the storage object to use
     */
    public GuardViewController(final Storage storage) {
        super(storage);
    }

    @Override
    public Optional<Guard> search(final int id) {
        return getStorage()
                .getGuards()
                .stream()
                .filter(g -> g.getGuardID() == id)
                .findFirst();
    }
}
