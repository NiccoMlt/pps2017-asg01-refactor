package controller.view.gui.view;

import controller.Storage;
import controller.view.TableViewController;
import controller.view.gui.AbstractGuiController;
import model.Cell;
import view.TableView;

import java.util.Collection;

/** Controller class for a view that graphically represents all the Cells in a table. */
public class CellsViewController extends AbstractGuiController<TableView<Cell>> implements TableViewController<Cell> {

    /**
     * Constructor.
     *
     * @param storage the storage object to use
     */
    public CellsViewController(final Storage storage) {
        super(storage);
    }

    @Override
    public Collection<Cell> get() {
        return getStorage().getCells();
    }
}
