package controller.view.gui.view;

import controller.Storage;
import controller.view.TableViewController;
import controller.view.gui.AbstractGuiController;
import model.people.Prisoner;
import view.TableView;

import java.util.Collection;

/** Controller class for a view that graphically represents all the prisoners in a table. */
public class PrisonersViewController
        extends AbstractGuiController<TableView<Prisoner>> implements TableViewController<Prisoner> {

    /**
     * Constructor.
     *
     * @param storage the storage object to use
     */
    public PrisonersViewController(final Storage storage) {
        super(storage);
    }

    @Override
    public Collection<Prisoner> get() {
        return getStorage().getPrisoners();
    }

}
