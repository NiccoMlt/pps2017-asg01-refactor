package controller.view;

import view.ObjectView;

import java.util.Optional;

/**
 * Interface of a controller for a View that shows something about a model object.
 *
 * @param <T> {@inheritDoc}
 */
public interface ObjectViewController<T> extends ViewController<ObjectView<T>> {

    /**
     * Search for an object matching that ID.
     *
     * @param id the identifier
     * @return the object, if any
     */
    Optional<T> search(int id);
}
