package controller.view;

import model.Cell;
import model.Movement;
import model.people.Guard;
import model.people.Prisoner;
import model.people.Visitor;
import view.ObjectInsertionView;
import view.ObjectRemovalView;
import view.ObjectView;
import view.PrisonView;
import view.TableView;
import view.gui.components.BarChart;
import view.gui.components.PieChart;

/** ViewController interface that implements a controller for the navigation menu of the application. */
public interface MenuController extends ViewController<PrisonView> {

    /**
     * Get a {@link ViewController} for a {@link ObjectInsertionView} to insert a {@link Prisoner}.
     *
     * @return the controller
     */
    InsertionController<Prisoner> addPrisoner();

    /**
     * Get a {@link ViewController} for a {@link ObjectRemovalView} to remove a {@link Prisoner}.
     *
     * @return the controller
     */
    RemovalController<Prisoner> removePrisoner();

    /**
     * Get a {@link ViewController} for a {@link ObjectView} representing a {@link Prisoner}.
     *
     * @return the controller
     */
    ObjectViewController<Prisoner> viewPrisoner();

    /**
     * Get a {@link ViewController} for a {@link ObjectInsertionView} to insert a {@link Movement}.
     *
     * @return the controller
     */
    InsertionController<Movement> addMovement();

    /**
     * Get a {@link ViewController} for a {@link TableView} representing {@link Movement Movements}.
     *
     * @return the controller
     */
    TableViewController<Movement> viewBalance();

    /** Build a {@link BarChart} frame to show the statistics of prisoners per year. */
    void buildBarChart();

    /** Build a {@link PieChart} frame to show the statistics of crimes per prisoner. */
    void buildPieChart();

    /**
     * Get a {@link ViewController} for a {@link ObjectInsertionView} to insert a {@link Visitor}.
     *
     * @return the controller
     */
    InsertionController<Visitor> addVisitor();

    /**
     * Get a {@link ViewController} for a {@link TableView} representing {@link Visitor Visitors}.
     *
     * @return the controller
     */
    TableViewController<Visitor> viewVisitors();

    /**
     * Get a {@link ViewController} for a {@link TableView} representing {@link Cell Cells}.
     *
     * @return the controller
     */
    TableViewController<Cell> viewCells();

    /**
     * Get a {@link ViewController} for a {@link ObjectInsertionView} to insert a {@link Guard}.
     *
     * @return the controller
     */
    InsertionController<Guard> addGuard();

    /**
     * Get a {@link ViewController} for a {@link ObjectRemovalView} to remove a {@link Guard}.
     *
     * @return the controller
     */
    RemovalController<Guard> removeGuard();

    /**
     * Get a {@link ViewController} for a {@link ObjectView} representing a {@link Guard}.
     *
     * @return the controller
     */
    ObjectViewController<Guard> viewGuard();

    /**
     * Get a {@link ViewController} for a {@link TableView} representing {@link Prisoner Prisoners}.
     *
     * @return the controller
     */
    TableViewController<Prisoner> viewPrisoners();
}
