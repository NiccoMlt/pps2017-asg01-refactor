package controller.view;

import view.TableView;

import java.util.Collection;

/**
 * Interface of a controller for a View that shows many model objects of the same type.
 *
 * @param <T> {@inheritDoc}
 */
public interface TableViewController<T> extends ViewController<TableView<T>> {

    /**
     * Get the objects to show.
     *
     * @return the objects to show
     */
    Collection<T> get();
}
