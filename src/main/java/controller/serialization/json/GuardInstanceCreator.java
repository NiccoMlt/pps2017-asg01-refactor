package controller.serialization.json;

import com.google.gson.InstanceCreator;
import model.people.Guard;
import model.people.GuardImpl;
import model.people.PersonBuilder;

import java.lang.reflect.Type;
import java.time.LocalDate;

/** Create instances of {@link Guard} interface with {@link GuardImpl} instances given by {@link PersonBuilder}. */
class GuardInstanceCreator implements InstanceCreator<Guard> {

    @Override
    public Guard createInstance(final Type type) {
        return new PersonBuilder("", "", LocalDate.now())
                .buildGuard(0, "", 0, "")
                .build();
    }
}
