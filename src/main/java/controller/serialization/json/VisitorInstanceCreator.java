package controller.serialization.json;

import com.google.gson.InstanceCreator;
import model.people.PersonBuilder;
import model.people.Visitor;
import model.people.VisitorImpl;

import java.lang.reflect.Type;
import java.time.LocalDate;

/** Create instances of {@link Visitor} interface with {@link VisitorImpl} instances given by {@link PersonBuilder}. */
class VisitorInstanceCreator implements InstanceCreator<Visitor> {

    @Override
    public Visitor createInstance(final Type type) {
        return new PersonBuilder("", "", LocalDate.MIN)
                .buildVisitor(0)
                .build();
    }
}
