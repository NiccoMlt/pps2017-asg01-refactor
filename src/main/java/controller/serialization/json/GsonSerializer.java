package controller.serialization.json;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.typeadapters.RuntimeTypeAdapterFactory;
import controller.serialization.Serializer;
import model.Cell;
import model.CellImpl;
import model.Movement;
import model.MovementImpl;
import model.people.Guard;
import model.people.GuardImpl;
import model.people.Person;
import model.people.PersonImpl;
import model.people.Prisoner;
import model.people.PrisonerImpl;
import model.people.Visitor;
import model.people.VisitorImpl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

/** Implementation of the serializer strategy interface that uses Google Gson to serialize objects to JSON format. */
public class GsonSerializer implements Serializer {

    private final Gson gson;

    /** Constructor that registers all needed type adapters. */
    public GsonSerializer() {
        gson = new GsonBuilder()
                .registerTypeAdapterFactory(RuntimeTypeAdapterFactory
                        .of(Person.class)
                        .registerSubtype(PersonImpl.class)
                        .registerSubtype(Guard.class)
                        .registerSubtype(GuardImpl.class)
                        .registerSubtype(Visitor.class)
                        .registerSubtype(VisitorImpl.class)
                        .registerSubtype(Prisoner.class)
                        .registerSubtype(PrisonerImpl.class))
                .registerTypeAdapterFactory(RuntimeTypeAdapterFactory
                        .of(Guard.class)
                        .registerSubtype(GuardImpl.class))
                .registerTypeAdapterFactory(RuntimeTypeAdapterFactory
                        .of(Visitor.class)
                        .registerSubtype(VisitorImpl.class))
                .registerTypeAdapterFactory(RuntimeTypeAdapterFactory
                        .of(Prisoner.class)
                        .registerSubtype(PrisonerImpl.class))
                .registerTypeAdapterFactory(RuntimeTypeAdapterFactory
                        .of(Cell.class)
                        .registerSubtype(CellImpl.class))
                .registerTypeAdapterFactory(RuntimeTypeAdapterFactory
                        .of(Movement.class)
                        .registerSubtype(MovementImpl.class))
                .registerTypeAdapter(Guard.class, new GuardInstanceCreator())
                .registerTypeAdapter(Visitor.class, new VisitorInstanceCreator())
                .registerTypeAdapter(Prisoner.class, new PrisonerInstanceCreator())
                .setPrettyPrinting()
                .create();
    }

    @Override
    public <T> T deserializeJSON(final Type clazz, final String path) throws IOException {
        final Reader reader = new InputStreamReader(new FileInputStream(path), StandardCharsets.UTF_8);
        final Optional<T> deserialize = Optional.ofNullable(gson.fromJson(reader, clazz));
        reader.close();
        return deserialize.orElseThrow(() -> new JsonIOException("The file provided is corrupted or non valid"));
    }

    @Override
    public <T> void serializeJSON(final T object, final Type clazz, final String path) throws IOException {
        final Writer writer = new OutputStreamWriter(new FileOutputStream(new File(path)), StandardCharsets.UTF_8);
        gson.toJson(object, clazz, writer);
        writer.close();
    }

}
