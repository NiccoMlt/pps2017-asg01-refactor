package controller.serialization.json;

import com.google.gson.InstanceCreator;
import model.people.PersonBuilder;
import model.people.Prisoner;
import model.people.PrisonerImpl;

import java.lang.reflect.Type;
import java.time.LocalDate;

/**
 * Create instances of {@link Prisoner} interface with {@link PrisonerImpl} instances given by {@link PersonBuilder}.
 */
class PrisonerInstanceCreator implements InstanceCreator<Prisoner> {

    @Override
    public Prisoner createInstance(final Type type) {
        return new PersonBuilder("", "", LocalDate.MIN)
                .buildPrisoner(0, 0, LocalDate.MIN, LocalDate.MIN)
                .build();
    }
}
