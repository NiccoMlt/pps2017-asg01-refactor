package controller.serialization;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import controller.Storage;
import model.Cell;
import model.Movement;
import model.people.Guard;
import model.people.Prisoner;
import model.people.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * Implementation of storage interface that delegates the serialization strategy to another class and defines methods
 * to handle the needed object types.
 */
public class SerializationStorage implements Storage {
    private static final Logger L = LoggerFactory.getLogger(SerializationStorage.class);

    // All default paths
    private static final Path HERE = Paths.get(".").toAbsolutePath().normalize();
    /** Resource folder used. */
    public static final String RES = HERE.toString() + File.separator + "res";
    private static final String EXT = ".json";
    /** Resource file used to store {@link Guard Guards}. */
    public static final String GUARDS = RES + File.separator + "GuardieUserPass" + EXT;
    /** Resource file used to store {@link Cell Cells}. */
    public static final String CELLS = RES + File.separator + "Celle" + EXT;
    private static final String PRISONERS = RES + File.separator + "Prisoners" + EXT;
    private static final String CURRENT_PRISONERS = RES + File.separator + "CurrentPrisoners" + EXT;
    private static final String MOVEMENTS = RES + File.separator + "AllMovements" + EXT;
    private static final String VISITORS = RES + File.separator + "Visitors" + EXT;

    // All Types
    private static final Type LIST_OF_GUARDS = new TypeToken<List<Guard>>() {}.getType();
    private static final Type LIST_OF_MOVEMENTS = new TypeToken<List<Movement>>() {}.getType();
    private static final Type LIST_OF_VISITORS = new TypeToken<List<Visitor>>() {}.getType();
    private static final Type LIST_OF_PRISONERS = new TypeToken<List<Prisoner>>() {}.getType();
    private static final Type LIST_OF_CELLS = new TypeToken<List<Cell>>() {}.getType();

    private final Serializer serializer;

    /**
     * Default constructor.
     *
     * @param serializer the serialization strategy object
     */
    public SerializationStorage(final Serializer serializer) {
        this.serializer = serializer;
    }

    /**
     * Check existence of the resource folder and access right, trying to create it if not present.
     *
     * @throws FileNotFoundException if it can't access the folder
     */
    private static void checkResExistence() throws FileNotFoundException {
        final File folder = new File(RES);
        if (!folder.isDirectory() && !folder.mkdir()) {
            throw new FileNotFoundException("The game's default directory is a file");
        }
    }

    /**
     * Logs the error message.
     *
     * @param e the exception to print
     */
    private static void handleError(final Throwable e) {
        L.error(e.getLocalizedMessage(), e);
    }

    /**
     * {@link Serializer#deserializeJSON(Type, String) Deserialize} a generic {@link Type} of object from JSON file
     * if it exists, or return a default value otherwise.
     *
     * @param clazz           the Type of object to deserialize
     * @param path            the Path where to find the file
     * @param defaultSupplier the supplier of default objects for the give Type
     * @param <T>             the generic Type of object
     * @return the deserialized object if the file is present, a default otherwise
     * @throws FileNotFoundException if the file does not exist, is a directory rather than a regular file,
     *                               or for some other reason cannot be opened for reading
     * @throws SecurityException     if a security manager exists and its checkRead method denies read access
     * @throws JsonIOException       if there was a problem reading from the Reader
     * @throws JsonSyntaxException   if json is not a valid representation for an object of type
     * @throws IOException           if an I/O error occurs
     */
    private <T> T checkExistenceAndDeserialize(final Type clazz, final String path,
                                               final Supplier<T> defaultSupplier) throws IOException {
        final File f = new File(path);
        if (f.exists() && f.isFile() && f.canRead()) {
            return this.serializer.deserializeJSON(clazz, path);
        } else {
            return defaultSupplier.get();
        }
    }

    @Override
    public List<Prisoner> getCurrentPrisoners() {
        try {
            return checkExistenceAndDeserialize(LIST_OF_PRISONERS, CURRENT_PRISONERS, ArrayList::new);
        } catch (final IOException e) {
            handleError(e);
            return new ArrayList<>();
        }
    }

    @Override
    public void setCurrentPrisoners(final List<Prisoner> prisoners) {
        try {
            checkResExistence();
            this.serializer.serializeJSON(prisoners, LIST_OF_PRISONERS, CURRENT_PRISONERS);
        } catch (final IOException e) {
            handleError(e);
        }
    }

    @Override
    public List<Prisoner> getPrisoners() {
        try {
            return checkExistenceAndDeserialize(LIST_OF_PRISONERS, PRISONERS, ArrayList::new);
        } catch (final IOException e) {
            handleError(e);
            return new ArrayList<>();
        }
    }

    @Override
    public void setPrisoners(final List<Prisoner> prisoners) {
        try {
            checkResExistence();
            this.serializer.serializeJSON(prisoners, LIST_OF_PRISONERS, PRISONERS);
        } catch (final IOException e) {
            handleError(e);
        }
    }

    @Override
    public List<Visitor> getVisitors() {
        try {
            return checkExistenceAndDeserialize(LIST_OF_VISITORS, VISITORS, ArrayList::new);
        } catch (final IOException e) {
            handleError(e);
            return new ArrayList<>();
        }
    }

    @Override
    public void setVisitors(final List<Visitor> visitors) {
        try {
            checkResExistence();
            this.serializer.serializeJSON(visitors, LIST_OF_VISITORS, VISITORS);
        } catch (final IOException e) {
            handleError(e);
        }
    }

    @Override
    public List<Guard> getGuards() {
        try {
            return checkExistenceAndDeserialize(LIST_OF_GUARDS, GUARDS, ArrayList::new);
        } catch (final IOException e) {
            handleError(e);
            return new ArrayList<>();
        }
    }

    @Override
    public void setGuards(final List<Guard> guards) {
        try {
            checkResExistence();
            this.serializer.serializeJSON(guards, LIST_OF_GUARDS, GUARDS);
        } catch (final IOException e) {
            handleError(e);
        }
    }

    @Override
    public List<Cell> getCells() {
        try {
            return checkExistenceAndDeserialize(LIST_OF_CELLS, CELLS, ArrayList::new);
        } catch (final IOException e) {
            handleError(e);
            return new ArrayList<>();
        }
    }

    @Override
    public void setCells(final List<Cell> cells) {
        try {
            checkResExistence();
            this.serializer.serializeJSON(cells, LIST_OF_CELLS, CELLS);
        } catch (final IOException e) {
            handleError(e);
        }
    }

    @Override
    public List<Movement> getMovements() {
        try {
            return checkExistenceAndDeserialize(LIST_OF_MOVEMENTS, MOVEMENTS, ArrayList::new);
        } catch (final IOException e) {
            handleError(e);
            return new ArrayList<>();
        }
    }

    @Override
    public void setMovements(final List<Movement> movements) {
        try {
            checkResExistence();
            this.serializer.serializeJSON(movements, LIST_OF_MOVEMENTS, MOVEMENTS);
        } catch (final IOException e) {
            handleError(e);
        }
    }

    @Override
    public void addCurrentPrisoner(final Prisoner prisoner) {
        final List<Prisoner> prisoners = getCurrentPrisoners();
        prisoners.add(prisoner);
        setCurrentPrisoners(prisoners);
    }

    @Override
    public void addPrisoner(final Prisoner prisoner) {
        final List<Prisoner> prisoners = getPrisoners();
        prisoners.add(prisoner);
        setPrisoners(prisoners);
    }

    @Override
    public void addVisitor(final Visitor visitor) {
        final List<Visitor> visitors = getVisitors();
        visitors.add(visitor);
        setVisitors(visitors);
    }

    @Override
    public void addGuard(final Guard guard) {
        final List<Guard> guards = getGuards();
        guards.add(guard);
        setGuards(guards);
    }

    @Override
    public void addToCell(final Prisoner prisoner) {
        final List<Cell> cells = getCells();
        final Optional<Cell> cell = cells
                .stream()
                .filter(c -> c.getId() == prisoner.getCellID())
                .findFirst();
        cell.ifPresent(c -> {
            final int currentPrisoners = c.getCurrentPrisoners();
            if (c.getCapacity() >= currentPrisoners + 1) {
                c.setCurrentPrisoners(currentPrisoners + 1);
                setCells(cells);
                addPrisoner(prisoner);
                addCurrentPrisoner(prisoner);
            }
        });

    }

    @Override
    public void removeFromCell(final Prisoner prisoner) {
        final List<Prisoner> currentPrisoners = getCurrentPrisoners();
        final List<Cell> cells = getCells();
        final Optional<Cell> cell = cells.stream().filter(c -> c.getId() == prisoner.getCellID()).findFirst();

        cell.ifPresent(c -> {
            currentPrisoners.remove(prisoner);
            c.setCurrentPrisoners(c.getCurrentPrisoners() - 1);
            setCells(cells);
            setCurrentPrisoners(currentPrisoners);
        });
    }

    @Override
    public void addMovement(final Movement movement) {
        final List<Movement> movements = getMovements();
        movements.add(movement);
        setMovements(movements);
    }
}
