package controller.serialization;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;

/** The interface models a strategy object that handles the serialization of generic types somehow. */
public interface Serializer {

    /**
     * Deserialize a generic {@link Type} of object from JSON file.
     *
     * @param clazz the Type of object to deserialize
     * @param path  the Path where to find the file
     * @param <T>   the generic Type of object
     * @return the deserialized object
     * @throws FileNotFoundException if the file does not exist, is a directory rather than a regular file,
     *                               or for some other reason cannot be opened for reading
     * @throws SecurityException     if a security manager exists and its checkRead method denies read access
     * @throws JsonIOException       if there was a problem reading from the Reader
     * @throws JsonSyntaxException   if json is not a valid representation for an object of type
     * @throws IOException           if an I/O error occurs
     */
    <T> T deserializeJSON(Type clazz, String path) throws IOException;

    /**
     * Serialize a generic {@link Type} of object from JSN file.
     *
     * @param object the object to serialize
     * @param clazz  the Type of object to serialize
     * @param path   the Path where to save the file
     * @param <T>    the generic Type of object
     * @throws SecurityException     if a security manager exists and denies access
     * @throws FileNotFoundException if the file exists but is a directory rather than a regular file,
     *                               does not exist but cannot be created, or cannot be opened for any other reason
     * @throws JsonIOException       if there was a problem writing to the writer
     * @throws IOException           if an I/O error occurs
     */
    <T> void serializeJSON(T object, Type clazz, String path) throws IOException;

}
