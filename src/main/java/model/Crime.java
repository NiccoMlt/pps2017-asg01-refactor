package model;

import model.people.Prisoner;

/** Enumeration that models the crimes committed by {@link Prisoner Prisoners}. */
public enum Crime {
    /** Reati contro gli animali. */
    ANIMALS("Reati contro gli animali"),
    /** Reati associativi. */
    ASSOCIATIVE("Reati associativi"),
    /** Blasfemia e sacrilegio. */
    BLASPHEMY("Blasfemia e sacrilegio"),
    /** Traffico di droga. */
    DRUG("Traffico di droga"),
    /** Reati economici e finanziari. */
    ECONOMY("Reati economici e finanziari"),
    /** Casi di truffe. */
    FRAUD("Casi di truffe"),
    /** Reati nell'ordinamento italiano. */
    ITALIAN("Reati nell'ordinamento italiano"),
    /** Falsa testimonianza. */
    LIE("Falsa testimonianza"),
    /** Reati militari. */
    MILITARY("Reati militari"),
    /** Reati contro il patrimonio. */
    PATRIMONY("Reati contro il patrimonio"),
    /** Reati contro la persona. */
    PERSON("Reati contro la persona"),
    /** Reati tributari. */
    TRIBUTARY("Reati tributari");

    private final String description;

    /**
     * Constructor.
     *
     * @param description a description of the crime
     */
    Crime(final String description) {
        this.description = description;
    }

    /**
     * Get the description of the crime.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * {@inheritDoc}
     *
     * @return {@inheritDoc}
     * @see #getDescription()
     */
    @Override
    public String toString() {
        return getDescription();
    }
}
