package model.people;

import java.time.LocalDate;

/** Implementation of Step Builder pattern that simplify building process of model classes in Person hierarchy. */
public class PersonBuilder extends PersonImpl.Builder<PersonBuilder> {

    /**
     * Constructor.
     * <p>
     * It takes all the mandatory parameters to build a {@link Person}.
     *
     * @param name      the name of the person
     * @param surname   the surname of the person
     * @param birthDate the birth date of the person
     */
    public PersonBuilder(final String name, final String surname, final LocalDate birthDate) {
        super(name, surname, birthDate);
    }

    @Override
    public Person build() {
        return new PersonImpl(this);
    }

    /**
     * Specify mandatory parameters to build a {@link Prisoner} {@link Person}.
     *
     * @param prisonerID the ID of the prisoner
     * @param cellID     the ID of the cell of the prisoner
     * @param start      the starting date of imprisonment
     * @param finish     the ending date of imprisonment
     * @return a new {@link PrisonerImpl.Builder}
     */
    public PrisonerImpl.Builder buildPrisoner(final int prisonerID, final int cellID,
                                              final LocalDate start, final LocalDate finish) {
        return new PrisonerImpl.Builder(this.build(), prisonerID, cellID, start, finish);
    }

    /**
     * Specify mandatory parameters to build a {@link Guard} {@link Person}.
     *
     * @param rank            the rank of the guard
     * @param telephoneNumber the phone number of the guard
     * @param guardID         the ID of the guard
     * @param password        the password of the guard
     */
    public GuardImpl.Builder buildGuard(final int rank, final String telephoneNumber, final int guardID, final String password) {
        return new GuardImpl.Builder(this.build(), rank, telephoneNumber, guardID, password);
    }

    /**
     * Specify mandatory parameters to build a {@link Visitor} {@link Person}.
     *
     * @param prisonerID the ID of the visited prisoner
     * @return a new {@link VisitorImpl.Builder}
     */
    public VisitorImpl.Builder buildVisitor(final int prisonerID) {
        return new VisitorImpl.Builder(this.build(), prisonerID);
    }

    @Override
    protected PersonBuilder self() {
        return this;
    }
}
