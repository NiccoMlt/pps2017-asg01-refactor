package model.people;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/** The interface models a person. */
public interface Person {

    /** The Simple {@link DateFormat} for a person-related date. */
    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy");
    /** The {@link DateTimeFormatter} for a person-related date. */
    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT.toLocalizedPattern());

    /**
     * Gets the name of the person.
     *
     * @return the name
     */
    String getName();

    /**
     * Gets the surname of the person.
     *
     * @return the surname
     */
    String getSurname();

    /**
     * Gets the birth date of the person.
     *
     * @return the birth date
     */
    LocalDate getBirthDate();
}