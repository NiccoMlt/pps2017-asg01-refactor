package model.people;

/** The interface models a visitor. */
public interface Visitor extends Person {

    /**
     * Gets the ID of the prisoner that the visitor met.
     *
     * @return the ID of the prisoner met
     */
    int getPrisonerID();
}