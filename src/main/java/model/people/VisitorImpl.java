package model.people;

import com.google.common.base.Objects;

import java.time.LocalDate;

/** Implementation of a Visitor. */
public class VisitorImpl extends PersonImpl implements Visitor {

    private final int prisonerID;

    /**
     * Private constructor for {@link Builder} class.
     *
     * @param builder the builder to take parameters from
     */
    private VisitorImpl(final Builder builder) {
        super(builder);
        this.prisonerID = builder.prisonerID;
    }

    @Override
    public int getPrisonerID() {
        return this.prisonerID;
    }

    @Override
    public String toString() {
        return super.toString() + ", " + "ID prigioniero visitato: " + getPrisonerID();
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code equals()} method using Intellij IDEA and Google Guava.
     *
     * @param o {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof VisitorImpl)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        } else {
            final Visitor visitor = (VisitorImpl) o;
            return getPrisonerID() == visitor.getPrisonerID();
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code hashCode()} method using Intellij IDEA and Google Guava.
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), getPrisonerID());
    }

    /** Builder class for {@link Visitor} objects. */
    public static final class Builder extends PersonImpl.Builder<Builder> {
        private final int prisonerID;

        /**
         * Constructor.
         *
         * @param name       the name of the person
         * @param surname    the surname of the person
         * @param birthDate  the birth date of the person
         * @param prisonerID the ID of the visited prisoner
         */
        Builder(final String name, final String surname, final LocalDate birthDate, final int prisonerID) {
            super(name, surname, birthDate);
            this.prisonerID = prisonerID;
        }

        /**
         * Construct the builder from existing {@link Person} object.
         *
         * @param person     person object to gather data from
         * @param prisonerID the ID of the visited prisoner
         */
        Builder(final Person person, final int prisonerID) {
            this(person.getName(), person.getSurname(), person.getBirthDate(), prisonerID);
        }

        @Override
        public Visitor build() {
            return new VisitorImpl(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}