package model.people;

/** The interface models a guard. */
public interface Guard extends Person {

    /** Minimum accepted password length. */
    int MIN_PASSWORD = 6;

    /** Default rank for wrong credentials authentication. */
    int NO_RANK = -1;

    /**
     * Gets the password of the guard.
     *
     * @return the password
     */
    String getPassword();

    /**
     * Gets the phone number of the guard.
     *
     * @return the phone number
     */
    String getTelephoneNumber();

    /**
     * Gets the rank of the guard.
     *
     * @return the rank
     */
    int getRank();

    /**
     * Gets the ID of the guard.
     *
     * @return the ID
     */
    int getGuardID();
}