package model.people;

import com.google.common.base.Objects;

import java.time.LocalDate;

/** Implementation of a {@code Person}. */
public class PersonImpl implements Person {

    private final String name;
    private final String surname;
    private final LocalDate birthDate;

    /**
     * Private constructor for {@link Builder} class.
     *
     * @param builder the builder to take parameters from
     */
    PersonImpl(final Builder builder) {
        this.name = builder.name;
        this.surname = builder.surname;
        this.birthDate = builder.birthDate;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public LocalDate getBirthDate() {
        return birthDate;
    }

    @Override
    public String toString() {
        final String className = getClass().getSimpleName().trim().isEmpty()
                ? getClass().getSimpleName()
                : "PersonImpl";

        return className + ": "
                + "nome: " + this.getName() + ", "
                + "cognome: " + this.getSurname() + ", "
                + "data di nascita: " + this.getBirthDate();
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code equals()} method using Intellij IDEA and Google Guava.
     *
     * @param o {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof PersonImpl)) {
            return false;
        } else {
            final Person person = (PersonImpl) o;
            return Objects.equal(getName(), person.getName()) &&
                    Objects.equal(getSurname(), person.getSurname()) &&
                    Objects.equal(getBirthDate(), person.getBirthDate());
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code hashCode()} method using Intellij IDEA and Google Guava.
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(getName(), getSurname(), getBirthDate());
    }

    /** Builder class for {@link Person} objects. */
    abstract static class Builder<T extends Builder<T>> {
        private final String name;
        private final String surname;
        private final LocalDate birthDate;

        /**
         * Constructor.
         * <p>
         * It takes all the mandatory parameters to build a {@link Person}.
         *
         * @param name      the name of the person
         * @param surname   the surname of the person
         * @param birthDate the birth date of the person
         */
        Builder(final String name, final String surname, final LocalDate birthDate) {
            this.name = name;
            this.surname = surname;
            this.birthDate = birthDate;
        }

        /**
         * Build the {@code Person}.
         *
         * @return a new {@code Person} instance
         */
        public abstract Person build();

        /**
         * Return the builder itself.
         * <p>
         * Should be implemented with a call to {@code this}.
         *
         * @return the builder itself
         */
        protected abstract T self();
    }
}
