package model.people;

import model.Cell;
import model.Crime;

import java.time.LocalDate;
import java.util.Set;

/**
 * The interface models a prisoner.
 */
public interface Prisoner extends Person {

    /**
     * Adds a crime to the prisoner's ones.
     *
     * @param crime the committed crime
     */
    void addCrime(Crime crime);

    /**
     * Gets the crimes committed by the prisoner.
     *
     * @return the crimes list
     */
    Set<Crime> getCrimes();

    /**
     * Gets the ID of the prisoner.
     *
     * @return the ID
     */
    int getPrisonerID();

    /**
     * Gets the start date of imprisonment.
     *
     * @return the start date
     */
    LocalDate getStart();

    /**
     * Gets the end date of imprisonment.
     *
     * @return the end date
     */
    LocalDate getFinish();

    /**
     * Gets the ID of the {@link Cell} the prisoner is imprisoned in.
     *
     * @return the ID of the {@link Cell}
     */
    int getCellID();
}