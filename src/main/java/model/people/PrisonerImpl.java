package model.people;

import com.google.common.base.Objects;
import model.Crime;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

/** Implementation of a Prisoner. */
public class PrisonerImpl extends PersonImpl implements Prisoner {

    private final int prisonerID;
    private final LocalDate start;
    private final EnumSet<Crime> crimes;
    private LocalDate finish;
    private int cellID;

    /**
     * Private constructor for {@link Builder} class.
     *
     * @param builder the builder to take parameters from
     */
    private PrisonerImpl(final Builder builder) {
        super(builder);
        this.prisonerID = builder.prisonerID;
        this.start = builder.start;
        this.finish = builder.finish;
        this.crimes = builder.crimes.clone();
        this.cellID = builder.cellID;
    }

    @Override
    public void addCrime(final Crime crime) {
        crimes.add(crime);
    }

    @Override
    public Set<Crime> getCrimes() {
        return Collections.unmodifiableSet(this.crimes);
    }

    @Override
    public int getPrisonerID() {
        return this.prisonerID;
    }

    @Override
    public LocalDate getStart() {
        return this.start;
    }

    @Override
    public LocalDate getFinish() {
        return finish;
    }

    @Override
    public int getCellID() {
        return cellID;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + "ID: " + prisonerID + ", "
                + "inizio prigionia: " + start + ", "
                + "fine prigionia: " + finish + ", "
                + "crimini: " + crimes;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code equals()} method using Intellij IDEA and Google Guava.
     *
     * @param o {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof PrisonerImpl)) {
            return false;
        } else if (!super.equals(o)) {
            return false;
        } else {
            final Prisoner prisoner = (PrisonerImpl) o;
            return getPrisonerID() == prisoner.getPrisonerID() &&
                    getCellID() == prisoner.getCellID() &&
                    Objects.equal(getStart(), prisoner.getStart()) &&
                    Objects.equal(getCrimes(), prisoner.getCrimes()) &&
                    Objects.equal(getFinish(), prisoner.getFinish());
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code hashCode()} method using Intellij IDEA and Google Guava.
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), getPrisonerID(), getStart(), getCrimes(), getFinish(), getCellID());
    }

    /** Builder class for {@link Prisoner} objects. */
    public static final class Builder extends PersonImpl.Builder<Builder> {
        private final int prisonerID;
        private final LocalDate start;
        private final EnumSet<Crime> crimes;
        private LocalDate finish;
        private int cellID;

        /**
         * Constructor.
         *
         * @param name       the name of the person
         * @param surname    the surname of the person
         * @param prisonerID the ID of the prisoner
         * @param cellID     the ID of the cell of the prisoner
         * @param start      the starting date of imprisonment
         * @param finish     the ending date of imprisonment
         */
        public Builder(final String name, final String surname, final LocalDate birthDate,
                       final int prisonerID, final int cellID, final LocalDate start, final LocalDate finish) {
            super(name, surname, birthDate);
            this.prisonerID = prisonerID;
            this.cellID = cellID;
            this.start = start;
            this.finish = finish;
            this.crimes = EnumSet.noneOf(Crime.class);
        }

        /**
         * Construct the builder from existing {@link Person} object.
         *
         * @param person     person object to gather data from
         * @param prisonerID the ID of the prisoner
         * @param cellID     the ID of the cell of the prisoner
         * @param start      the starting date of imprisonment
         * @param finish     the ending date of imprisonment
         */
        public Builder(final Person person, final int prisonerID, final int cellID,
                       final LocalDate start, final LocalDate finish) {
            this(person.getName(), person.getSurname(), person.getBirthDate(), prisonerID, cellID, start, finish);
        }

        /**
         * Add a {@link Crime} to the {@link Prisoner} ones.
         *
         * @param crime the crime to add
         * @return the builder itself
         */
        public Builder addCrime(final Crime crime) {
            this.crimes.add(crime);
            return self();
        }

        /**
         * Add a {@link Collection} of {@link Crime Crimes} to the {@link Prisoner} ones.
         *
         * @param crimes the crimes to add
         * @return the builder itself
         */
        public Builder addCrimes(final Collection<Crime> crimes) {
            this.crimes.addAll(crimes);
            return self();
        }

        @Override
        public Prisoner build() {
            return new PrisonerImpl(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}