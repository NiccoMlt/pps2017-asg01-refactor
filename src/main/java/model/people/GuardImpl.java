package model.people;

import com.google.common.base.Objects;

import java.time.LocalDate;

/** Implementation of a guard. */
public class GuardImpl extends PersonImpl implements Guard {

    private final int guardID;
    private int rank;
    private String telephoneNumber;
    private String password;

    /**
     * Private constructor for {@link Builder} class.
     *
     * @param builder the builder to take parameters from
     */
    private GuardImpl(final Builder builder) {
        super(builder);
        this.guardID = builder.idGuardia;
        this.password = builder.password;
        this.rank = builder.rank;
        this.telephoneNumber = builder.telephoneNumber;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    @Override
    public int getRank() {
        return rank;
    }

    @Override
    public int getGuardID() {
        return guardID;
    }

    @Override
    public String toString() {
        return super.toString() + ", "
                + "rank: " + rank + ", "
                + "numero di telefono: " + telephoneNumber + ", "
                + "ID: " + guardID
                + "password: " + password;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code equals()} method using Intellij IDEA and Google Guava.
     *
     * @param o {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof GuardImpl)) {
            return false;
        } else if (!super.equals(o)) {
            return false;
        } else {
            final Guard guard = (GuardImpl) o;
            return getGuardID() == guard.getGuardID() &&
                    getRank() == guard.getRank() &&
                    Objects.equal(getTelephoneNumber(), guard.getTelephoneNumber()) &&
                    Objects.equal(getPassword(), guard.getPassword());
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code hashCode()} method using Intellij IDEA and Google Guava.
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), getGuardID(), getRank(), getTelephoneNumber(), getPassword());
    }

    /** Builder class for {@link Prisoner} objects. */
    public static class Builder extends PersonImpl.Builder<Builder> {

        private final int idGuardia;
        private int rank;
        private String telephoneNumber;
        private String password;

        /**
         * Constructor.
         *
         * @param name            the name of the person
         * @param surname         the surname of the person
         * @param birthDate       the birth date of the person
         * @param rank            the rank of the guard
         * @param telephoneNumber the phone number of the guard
         * @param guardID         the ID of the guard
         * @param password        the password of the guard
         */
        Builder(final String name, final String surname, final LocalDate birthDate, final int rank,
                final String telephoneNumber, final int guardID, final String password) {
            super(name, surname, birthDate);
            this.rank = rank;
            this.telephoneNumber = telephoneNumber;
            this.idGuardia = guardID;
            this.password = password;
            if (this.password.length() < MIN_PASSWORD) {
                throw new IllegalArgumentException("La password è più corta di " + MIN_PASSWORD + " caratteri");
            }
        }

        /**
         * Construct the builder from existing {@link Person} object.
         *
         * @param person          person object to gather data from
         * @param rank            the rank of the guard
         * @param telephoneNumber the phone number of the guard
         * @param guardID         the ID of the guard
         * @param password        the password of the guard
         */
        Builder(final Person person, final int rank, final String telephoneNumber,
                final int guardID, final String password) {
            this(person.getName(), person.getSurname(), person.getBirthDate(),
                    rank, telephoneNumber, guardID, password);
        }

        @Override
        public Guard build() {
            return new GuardImpl(this);
        }

        @Override
        protected Builder self() {
            return this;
        }
    }
}