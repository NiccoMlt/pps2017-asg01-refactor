package model;

import com.google.common.base.Objects;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/** Implementation of a movement. */
public class MovementImpl implements Movement {

    private final String description;
    private final BigDecimal amount;
    private final LocalDateTime date;

    /**
     * Constructor.
     *
     * @param description the description of the movement
     * @param amount      the amount of money moved
     */
    public MovementImpl(final String description, final BigDecimal amount) {
        this(description, amount, LocalDateTime.now());
    }

    /**
     * Constructor.
     *
     * @param description the description of the movement
     * @param amount      the amount of money moved
     * @param date        the date the movement was done
     */
    public MovementImpl(final String description, final BigDecimal amount, final LocalDateTime date) {
        this.amount = amount;
        this.description = description;
        this.date = date;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public BigDecimal getAmount() {
        return this.amount;
    }

    @Override
    public LocalDateTime getDate() {
        return this.date;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code equals()} method using Intellij IDEA and Google Guava.
     *
     * @param o {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof MovementImpl)) {
            return false;
        } else {
            final Movement movement = (MovementImpl) o;
            return Objects.equal(getDescription(), movement.getDescription()) &&
                    Objects.equal(getAmount(), movement.getAmount()) &&
                    Objects.equal(getDate(), movement.getDate());
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code hashCode()} method using Intellij IDEA and Google Guava.
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(getDescription(), getAmount(), getDate());
    }

    @Override
    public String toString() {
        return "Movimento: "
                + "descrizione: " + description + ", "
                + "ammontare: " + amount.toPlainString() + ", "
                + "data: " + date.format(Movement.DATE_TIME_FORMATTER);
    }
}
