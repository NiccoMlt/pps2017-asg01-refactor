package model;

import com.google.common.base.Objects;

/** Implementation of a cell. */
public class CellImpl implements Cell {

    private final int id;
    private final int capacity;
    private String description;
    private int currentPrisoners;

    /**
     * Constructor.
     *
     * @param id          the ID of the cell
     * @param description the description of the cell
     * @param capacity    the capacity of the cell
     */
    public CellImpl(final int id, final String description, final int capacity) {
        this.id = id;
        this.description = description;
        this.capacity = capacity;
        this.currentPrisoners = 0;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(final String position) {
        this.description = position;
    }

    @Override
    public int getCapacity() {
        return capacity;
    }

    @Override
    public int getCurrentPrisoners() {
        return currentPrisoners;
    }

    @Override
    public void setCurrentPrisoners(final int currentPrisoners) {
        this.currentPrisoners = currentPrisoners;
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code equals()} method using Intellij IDEA and Google Guava.
     *
     * @param o {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        } else if (!(o instanceof CellImpl)) {
            return false;
        } else {
            final Cell cell = (CellImpl) o;
            return getId() == cell.getId() &&
                    getCapacity() == cell.getCapacity() &&
                    getCurrentPrisoners() == cell.getCurrentPrisoners() &&
                    Objects.equal(getDescription(), cell.getDescription());
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * Automatically generated {@code hashCode()} method using Intellij IDEA and Google Guava.
     *
     * @return {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return Objects.hashCode(getId(), getCapacity(), getDescription(), getCurrentPrisoners());
    }

    @Override
    public String toString() {
        return "CellImpl: "
                + "ID: " + id + ", "
                + "posizione: " + description + ", "
                + "capacità: " + capacity + ", "
                + "prigionieri correnti: " + currentPrisoners;
    }
}
