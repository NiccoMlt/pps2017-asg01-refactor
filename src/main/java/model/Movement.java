package model;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/** The interface models a movement of the balance. */
public interface Movement {

    /** The Simple {@link DateFormat} for a movement-related date. */
    SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    /** The {@link DateTimeFormatter} for a movement-related date. */
    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT.toLocalizedPattern());

    /**
     * Gets the description of the movement.
     *
     * @return the description
     */
    String getDescription();

    /**
     * Gets the amount of money moved.
     *
     * @return the amount moved
     */
    BigDecimal getAmount();

    /**
     * Gets the date the movement was done.
     *
     * @return the date
     */
    LocalDateTime getDate();
}