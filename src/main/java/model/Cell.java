package model;

/** The interface models a Cell. */
public interface Cell {

    /**
     * Gets the ID of the cell.
     *
     * @return the ID
     */
    int getId();

    /**
     * Gets a description of the cell (eg. the position).
     *
     * @return the description
     */
    String getDescription();

    /**
     * Sets the description of the cell (eg. the position).
     *
     * @param description the description to set
     */
    void setDescription(String description);

    /**
     * Gets the maximum capacity of the cell.
     *
     * @return the capacity
     */
    int getCapacity();

    /**
     * Gets the current number of prisoners imprisoned in the cell.
     *
     * @return the prisoners in the cell
     */
    int getCurrentPrisoners();

    /**
     * Sets the current number of prisoners imprisoned in the cell.
     *
     * @param currentPrisoners the number of prisoners to set
     */
    void setCurrentPrisoners(int currentPrisoners);
}
