package main;

import controller.Storage;
import controller.serialization.SerializationStorage;
import controller.serialization.json.GsonSerializer;
import model.Cell;
import model.CellImpl;
import model.people.Guard;
import model.people.Person;
import model.people.PersonBuilder;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/** Class that initializes the default resources. */
public class Initializer {

    /** Empty, private, constructor, as this is an utility class. */
    private Initializer() {
        throw new AssertionError("Suppress default constructor for noninstantiability");
    }

    /** Initialize default resources. */
    public static void init() throws IOException {
        final File folder = new File(SerializationStorage.RES);

        if ((!folder.exists() && folder.mkdir()) || (folder.exists() && folder.isDirectory() && folder.canRead())) {
            final File fg = new File(SerializationStorage.GUARDS);

            final Storage storage = new SerializationStorage(new GsonSerializer());
            if (fg.length() == 0) {
                initializeGuards(storage);
            }

            final File f = new File(SerializationStorage.CELLS);

            if (f.length() == 0) {
                initializeCells(storage);
            }
        } else {
            throw new IOException("Impossibile accedere alla cartella");
        }
    }

    /**
     * Initialize the cells.
     *
     * @param storage the storage object to use
     */
    private static void initializeCells(final Storage storage) {
        final List<Cell> list = new ArrayList<>();

        for (int i = 0; i < 50; i++) {
            final CellImpl c;
            if (i < 20) {
                c = new CellImpl(i, "Primo piano", 4);
            } else if (i < 40) {
                c = new CellImpl(i, "Secondo piano", 3);
            } else if (i < 45) {
                c = new CellImpl(i, "Terzo piano", 4);
            } else {
                c = new CellImpl(i, "Piano sotterraneo, celle di isolamento", 1);
            }
            list.add(c);
        }

        storage.setCells(list);
    }

    /**
     * Initialize the guards.
     *
     * @param storage the storage object to use
     */
    private static void initializeGuards(final Storage storage) {
        final List<Guard> list = new ArrayList<>();
        final LocalDate date = LocalDate.parse("01/01/1980", Person.DATE_TIME_FORMATTER);
        final Guard g1 = new PersonBuilder("Oronzo", "Cantani", date)
                .buildGuard(1, "0764568", 1, "ciao01")
                .build();
        list.add(g1);
        final Guard g2 = new PersonBuilder("Emile", "Heskey", date)
                .buildGuard(2, "456789", 2, "asdasd")
                .build();
        list.add(g2);
        final Guard g3 = new PersonBuilder("Gennaro", "Alfieri", date)
                .buildGuard(3, "0764568", 3, "qwerty")
                .build();
        list.add(g3);

        storage.setGuards(list);
    }
}

