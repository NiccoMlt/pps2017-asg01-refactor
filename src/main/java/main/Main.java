package main;

import controller.view.LoginController;
import controller.view.gui.LoginControllerImpl;
import view.Login;
import view.gui.menu.LoginView;

import java.io.IOException;

import static main.Initializer.init;

/** Main class of the application. */
public final class Main {

    /** Empty, private, constructor. */
    private Main() {
        throw new AssertionError("Suppress default constructor for noninstantiability");
    }

    /**
     * Main method of the application, it starts it.
     *
     * @param args unused, ignore
     */
    public static void main(final String... args) throws IOException {
        init();
        final LoginController controller = new LoginControllerImpl();
        final Login view = new LoginView(controller);
        controller.setView(view);
        view.displayInfoMessage("Consiglio",
                "Accedere con i profili: \n ID:3 , password:qwerty \n ID:2 , password:asdasd ");
    }
}
