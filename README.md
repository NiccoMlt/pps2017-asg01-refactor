# Assignment n.1 #

In this assignment, you are asked to put into practice what you have learnt about code quality and refactoring. You will start from a Java project of your choice, and improve/refactor it. You may also implement some new feature.

## Detailed instructions: ##

1. fork your starting repository into "pps2017-asg01-refactor";
2. identify portions of the code where you would like to improve things, apply patterns, add new features;
3. do the job;
4. the latex template of the Report you have to produce is attached, with detailed instructions on what to do and how to document it;
5. the Downloads section of your repository will need to include your PDF version of the Report (we strongly encourage you to write in English and use the provided Latex template, but will tolerate if you write in Italian and use a different word processor);
6. you should properly use git to track your changes: recall to commit small and meaningful changes (we may use commits also to check if you actually worked more or less for the right amount of time) -- recall not to track binaries;
7. deliver your assignment using this e-learning tool, by just providing the PDF report;
8. it is understood that you should not, for obvious reasons, modify the repo after you submitted;
9. if produced before the exam, this assignment will be used at the exam to check your knowledge on code quality, refactoring, and patterns: doing it properly will make the exam easier, doing it not so properly does not change things: you will be able to remedy at the exam so do not worry! 

_Suggestion_: exercise your skills now! 

## [Template](./template.pdf) suggestions: ##

- Produce a documentation according to this template. Try to keep this document in one page, within 4-5 hours of work, do the job yourself, deliver before the exam, consider the possibility of doing it this very week. Erase this and all other footnotes at the end before submitting.
- You have to select a starting Java project of your preference and fork it: it can be one of the three given at lab01, your solution of lab02, a previous project of yours, or others. Please name your project exactly pps2017-asg01-refactor, and perform meaningful commits so we can check what you had before and after refactoring. Be sure it is visible and do not modify it after submission.
- As example we here indicated a possible lists of changes. Please adapt as needed. Describe (3-4) changes, and the time it took to implement them. Possibly stay in chronological order. Consider applying some pattern.
- Possibly indicate 0-2 new features you added, with 2-3 lines each for description of changes in code. We remark that adding new features is not strictly required: you may also spend your time just improving existing code. If new features are added, do so with excellent code, and hopefully with patterns.
- Add a max 10 lines evaluation of this experience, reporting which principles and methodology you used, what went good, what went bad, and so on.

## Code repository: ##

[https://bitbucket.org/NiccoMlt/pps-lab-refactoring-prison](https://bitbucket.org/NiccoMlt/pps-lab-refactoring-prison)

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/3734a9525a34409c9b67b32bf627c7aa)](https://www.codacy.com/app/NiccoMlt/pps2017-asg01-refactor?utm_source=bitbucket.org&amp;utm_medium=referral&amp;utm_content=NiccoMlt/pps2017-asg01-refactor&amp;utm_campaign=Badge_Grade)
[![SonarCloud](https://sonarcloud.io/api/project_badges/measure?project=NiccoMlt_pps2017-asg01-refactor&metric=alert_status)](https://sonarcloud.io/dashboard?id=NiccoMlt_pps2017-asg01-refactor)

